﻿using System.Collections.Generic;
using System.Diagnostics;

#pragma warning disable 1591
namespace CatEars.Core.Collections
{
    /// <summary>
    /// 字典到集合,容器为List
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    [DebuggerDisplay("Count={InnerDict.Count}")]
    public class DictToCollection<TKey, TValue> :
        DictEx<TKey, ICollection<TValue>>,
        IDictionary<TKey, ICollection<TValue>>
    {
        /// <summary>
        /// 创建集合
        /// </summary>
        /// <returns></returns>
        protected virtual ICollection<TValue> CreateCollection()
        {
            return new List<TValue>();
        }

        /// <summary>
        /// 添加对象到集合
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="value"></param>
        protected virtual void AddValueToColl(ICollection<TValue> coll, TValue value)
        {
            coll.Add(value);
        }

        /// <summary>
        /// 添加对象到集合
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public int AddValue(TKey key, TValue value)
        {
            ICollection<TValue> coll = GetOrAddValue(key, CreateCollection);
            AddValueToColl(coll, value);
            return coll.Count;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ICollection<TValue> GetCollection(TKey key)
        {
            return this.GetOrDefault(key);
        }
    }

    /// <summary>
    /// 字典到集合,容器为List,跟DictToColl是一样的，只是指明了容器为List
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class DictToList<TKey, TValue> :
        DictToCollection<TKey, TValue>,
        IDictionary<TKey, ICollection<TValue>>
    {
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<TValue> GetList(TKey key)
        {
            return this.GetCollection(key) as List<TValue>;
        }
    }

    /// <summary>
    /// 字典到集合,容器为LinkedList
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class DictToLinkedList<TKey, TValue> :
        DictToCollection<TKey, TValue>,
        IDictionary<TKey, ICollection<TValue>>
    {
        /// <summary>
        /// 往前追加
        /// </summary>
        bool AppendToFirst { get; set; }

        /// <summary>
        /// 创建集合
        /// </summary>
        /// <returns></returns>
        protected override ICollection<TValue> CreateCollection()
        {
            return new LinkedList<TValue>();
        }

        /// <summary>
        /// 添加对象到集合
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="value"></param>
        protected override void AddValueToColl(ICollection<TValue> coll, TValue value)
        {
            var list = coll as LinkedList<TValue>;
            if (AppendToFirst)
            {
                list.AddFirst(value);
            }
            else
            {
                list.AddLast(value);
            }
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public LinkedList<TValue> GetList(TKey key)
        {
            return this.GetCollection(key) as LinkedList<TValue>;
        }
    }
}
