﻿using System;

namespace CatEars.Core.Collections
{
    /// <summary>
    /// 数组功能扩展
    /// </summary>
    public static partial class EmptyArray
    {
        /// <summary>
        /// 空 Object 数组
        /// </summary>
        public static Object[] ObjectArray = new Object[0];
        /// <summary>
        /// 空 Char 数组
        /// </summary>
        public static Char[] CharArray = new Char[0];
        /// <summary>
        /// 空 String 数组
        /// </summary>
        public static String[] StringArray = new String[0];
        /// <summary>
        /// 空 Byte 数组
        /// </summary>
        public static Byte[] ByteArray = new Byte[0];
        /// <summary>
        /// 空 SByte 数组
        /// </summary>
        public static SByte[] SByteArray = new SByte[0];
        /// <summary>
        /// 空 Int16 数组
        /// </summary>
        public static Int16[] Int16Array = new Int16[0];
        /// <summary>
        /// 空 UInt16 数组
        /// </summary>
        public static UInt16[] UInt16Array = new UInt16[0];
        /// <summary>
        /// 空 Int32 数组
        /// </summary>
        public static Int32[] Int32Array = new Int32[0];
        /// <summary>
        /// 空 UInt32 数组
        /// </summary>
        public static UInt32[] UInt32Array = new UInt32[0];
        /// <summary>
        /// 空 Int64 数组
        /// </summary>
        public static Int64[] Int64Array = new Int64[0];
        /// <summary>
        /// 空 UInt64 数组
        /// </summary>
        public static UInt64[] UInt64Array = new UInt64[0];
        /// <summary>
        /// 空 Single 数组
        /// </summary>
        public static Single[] SingleArray = new Single[0];
        /// <summary>
        /// 空 Double 数组
        /// </summary>
        public static Double[] DoubleArray = new Double[0];
        /// <summary>
        /// 空 Decimal 数组
        /// </summary>
        public static Decimal[] DecimalArray = new Decimal[0];
        /// <summary>
        /// 空 Type 数组
        /// </summary>
        public static Type[] TypeArray = new Type[0];

    }
}
