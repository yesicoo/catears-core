﻿
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CatEars.Core.Collections
{
    /// <summary>
    /// 固定大小历史集合（后添加的在前，去重，集合大小可固定）
    /// 不实现IEnumerable方便Json序列化
    /// </summary>
    [DebuggerDisplay("Count={Count} Capacity={Capacity}")]
    public class History<T>
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public History() : this(-1)
        {
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="capacity">容量 -1表示不限制</param>
        public History(int capacity)
            : this(capacity, null)
        {
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="capacity">容量 -1表示不限制</param>
        /// <param name="collection">初始化集合</param>
        public History(int capacity, IEnumerable<T> collection)
        {
            Capacity = capacity;
            if (collection != null)
            {
                _innerColl = new LinkedList<T>(collection);
                Resize();
            }
            else
            {
                _innerColl = new LinkedList<T>();
            }
        }

        /// <summary>
        /// 内部集合
        /// </summary>
        private LinkedList<T> _innerColl = null;

        /// <summary>
        /// 容量 负数表示不限制容量
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// 获取或设置历史集合[用于Json序列化]
        /// </summary>
        public T[] Items
        {
            get
            {
                return _innerColl.ToArray();
            }
            set
            {
                foreach (T entity in value)
                {
                    _innerColl.AddLast(entity);
                }
                Resize();
            }
        }

        /// <summary>
        /// 重置历史记录大小
        /// </summary>
        public void Resize()
        {
            if (Capacity < 0)
            {
                return;
            }
            else if (Capacity == 0)
            {
                _innerColl.Clear();
            }
            else
            {
                while (_innerColl.Count > Capacity)
                {
                    _innerColl.RemoveLast();
                }
            }
        }

        /// <summary>
        /// 添加对象
        /// </summary>
        /// <param name="item">对象</param>
        public void Add(T item)
        {
            if (Capacity == 0)
            {
                return;
            }
            bool blnRemove = _innerColl.Remove(item);
            _innerColl.AddFirst(item);
            if (!blnRemove)
            {
                Resize();
            }
        }

        /// <summary>
        /// 移除对象
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>返回值</returns>
        public bool Remove(T item)
        {
            return _innerColl.Remove(item);
        }

        /// <summary>
        /// 清除记录
        /// </summary>
        public void Clear()
        {
            _innerColl.Clear();
        }

        /// <summary>
        /// 判断是否包含某对象
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>返回值</returns>
        public bool Contains(T item)
        {
            return _innerColl.Contains(item);
        }

        /// <summary>
        /// 集合总数
        /// </summary>
        public int Count
        {
            get { return _innerColl.Count; }
        }
    }
}
