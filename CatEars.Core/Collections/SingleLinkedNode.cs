﻿using System.Diagnostics;

namespace CatEars.Core.Collections
{
    /// <summary>
    /// 链表节点
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DebuggerDisplay("Value={Value} Next={Next}")]
    public class SingleLinkedNode<T>
    {
        /// <summary>
        /// 取值
        /// </summary>
        public T Value { get; set; }

        /// <summary>
        /// 下一个
        /// </summary>
        public SingleLinkedNode<T> Next { get; set; }
    }
}
