﻿using System.Diagnostics;

#pragma warning disable 1591
namespace CatEars.Core.Collections
{
    #region DictEx
    [DebuggerDisplay("Count={InnerDict.Count}")]
    public class DictEx<TKey1, TKey2, TValue> : DictEx<TKey1, DictEx<TKey2, TValue>>
    {
        public DictEx()
        {
            Get = new Getter<TKey1, TKey2, TValue>(this);
            Set = new Setter<TKey1, TKey2, TValue>(this);
        }

        public Getter<TKey1, TKey2, TValue> Get { get; private set; }

        public Setter<TKey1, TKey2, TValue> Set { get; private set; }

        /// <summary>
        /// 创建下级字典
        /// </summary>
        /// <returns>DictEx</returns>
        internal static DictEx<TKey2, TValue> CreateSubDict()
        {
            return new DictEx<TKey2, TValue>();
        }
    }

    [DebuggerDisplay("Count={InnerDict.Count}")]
    public class DictEx<TKey1, TKey2, TKey3, TValue> : DictEx<TKey1, DictEx<TKey2, TKey3, TValue>>
    {
        public DictEx()
        {
            Get = new Getter<TKey1, TKey2, TKey3, TValue>(this);
            Set = new Setter<TKey1, TKey2, TKey3, TValue>(this);
        }

        public Getter<TKey1, TKey2, TKey3, TValue> Get { get; private set; }

        public Setter<TKey1, TKey2, TKey3, TValue> Set { get; private set; }

        /// <summary>
        /// 创建下级字典
        /// </summary>
        /// <returns>DictEx</returns>
        internal static DictEx<TKey2, TKey3, TValue> CreateSubDict()
        {
            return new DictEx<TKey2, TKey3, TValue>();
        }
    }

    [DebuggerDisplay("Count={InnerDict.Count}")]
    public class DictEx<TKey1, TKey2, TKey3, TKey4, TValue> : DictEx<TKey1, DictEx<TKey2, TKey3, TKey4, TValue>>
    {
        public DictEx()
        {
            Get = new Getter<TKey1, TKey2, TKey3, TKey4, TValue>(this);
            Set = new Setter<TKey1, TKey2, TKey3, TKey4, TValue>(this);
        }

        public Getter<TKey1, TKey2, TKey3, TKey4, TValue> Get { get; private set; }

        public Setter<TKey1, TKey2, TKey3, TKey4, TValue> Set { get; private set; }

        /// <summary>
        /// 创建下级字典
        /// </summary>
        /// <returns>DictEx</returns>
        internal static DictEx<TKey2, TKey3, TKey4, TValue> CreateSubDict()
        {
            return new DictEx<TKey2, TKey3, TKey4, TValue>();
        }
    }
    #endregion

    #region Getter
    public class Getter<TKey1, TKey2, TValue>
    {
        static DictEx<TKey2, TValue> _default;
        DictEx<TKey1, TKey2, TValue> _dict;

        static Getter()
        {
            _default = DictEx<TKey1, TKey2, TValue>.CreateSubDict();
        }

        internal Getter(DictEx<TKey1, TKey2, TValue> dict)
        {
            _dict = dict;
        }

        public DictEx<TKey2, TValue> this[TKey1 key1]
        {
            get
            {
                return _dict.GetOrDefault(key1, _default);
            }
        }
    }

    public class Getter<TKey1, TKey2, TKey3, TValue>
    {
        static DictEx<TKey2, TKey3, TValue> _default;
        DictEx<TKey1, TKey2, TKey3, TValue> _dict;

        static Getter()
        {
            _default = DictEx<TKey1, TKey2, TKey3, TValue>.CreateSubDict();
        }

        internal Getter(DictEx<TKey1, TKey2, TKey3, TValue> dict)
        {
            _dict = dict;
        }

        public Getter<TKey2, TKey3, TValue> this[TKey1 key1]
        {
            get
            {
                return _dict.GetOrDefault(key1, _default).Get;
            }
        }
    }

    public class Getter<TKey1, TKey2, TKey3, TKey4, TValue>
    {
        static DictEx<TKey2, TKey3, TKey4, TValue> _default;
        DictEx<TKey1, TKey2, TKey3, TKey4, TValue> _dict;

        static Getter()
        {
            _default = DictEx<TKey1, TKey2, TKey3, TKey4, TValue>.CreateSubDict();
        }

        internal Getter(DictEx<TKey1, TKey2, TKey3, TKey4, TValue> dict)
        {
            _dict = dict;
        }

        public Getter<TKey2, TKey3, TKey4, TValue> this[TKey1 key1]
        {
            get
            {
                return _dict.GetOrDefault(key1, _default).Get;
            }
        }
    }
    #endregion

    #region Setter

    public class Setter<TKey1, TKey2, TValue>
    {
        DictEx<TKey1, TKey2, TValue> _dict;

        internal Setter(DictEx<TKey1, TKey2, TValue> dict)
        {
            _dict = dict;
        }

        public DictEx<TKey2, TValue> this[TKey1 key1]
        {
            get
            {
                return _dict.GetOrAddValue(key1, DictEx<TKey1, TKey2, TValue>.CreateSubDict);
            }
        }
    }

    public class Setter<TKey1, TKey2, TKey3, TValue>
    {
        DictEx<TKey1, TKey2, TKey3, TValue> _dict;

        internal Setter(DictEx<TKey1, TKey2, TKey3, TValue> dict)
        {
            _dict = dict;
        }

        public Setter<TKey2, TKey3, TValue> this[TKey1 key1]
        {
            get
            {
                return _dict.GetOrAddValue(key1, DictEx<TKey1, TKey2, TKey3, TValue>.CreateSubDict).Set;
            }
        }
    }

    public class Setter<TKey1, TKey2, TKey3, TKey4, TValue>
    {
        DictEx<TKey1, TKey2, TKey3, TKey4, TValue> _dict;

        internal Setter(DictEx<TKey1, TKey2, TKey3, TKey4, TValue> dict)
        {
            _dict = dict;
        }

        public Setter<TKey2, TKey3, TKey4, TValue> this[TKey1 key1]
        {
            get
            {
                return _dict.GetOrAddValue(key1, DictEx<TKey1, TKey2, TKey3, TKey4, TValue>.CreateSubDict).Set;
            }
        }
    }
    #endregion
}
