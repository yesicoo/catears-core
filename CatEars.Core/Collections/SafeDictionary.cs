﻿using System.Collections.Generic;

#pragma warning disable 1591
namespace CatEars.Core.Collections
{
    public class SafeDictionary<TKey, TValue>
    {
        private readonly object _syncLock = new object();
        private readonly Dictionary<TKey, TValue> _innerDict;

        public SafeDictionary(int capacity)
        {
            _innerDict = new Dictionary<TKey, TValue>(capacity);
        }

        public SafeDictionary()
        {
            _innerDict = new Dictionary<TKey, TValue>();
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            lock (_syncLock)
                return _innerDict.TryGetValue(key, out value);
        }

        public int Count { get { lock (_syncLock) return _innerDict.Count; } }

        public TValue this[TKey key]
        {
            get
            {
                lock (_syncLock)
                    return _innerDict[key];
            }
            set
            {
                lock (_syncLock)
                    _innerDict[key] = value;
            }
        }

        public void Add(TKey key, TValue value)
        {
            lock (_syncLock)
            {
                if (_innerDict.ContainsKey(key) == false)
                    _innerDict.Add(key, value);
            }
        }
    }
}
