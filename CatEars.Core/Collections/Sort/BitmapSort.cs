﻿using System.Collections;
using System.Collections.Generic;

namespace CatEars.Core.Collections.Sort
{
    /// <summary>
    /// 位图排序
    /// </summary>
    public static class BitmapSort
    {
        /// <summary>
        /// 位图排序
        /// </summary>
        /// <param name="input">输入内容</param>
        /// <param name="minValue">输入内容的最小值</param>
        /// <param name="maxValue">输入内容的最大值</param>
        /// <returns>排序后的结果</returns>
        public static IEnumerable<int> Sort(
            IEnumerable<int> input, int minValue, int maxValue)
        {
            if (input == null) { yield break; }
            var offset = minValue;
            var length = maxValue - minValue + 1;
            BitArray buffer = new BitArray(length);
            foreach (var item in input)
            {
                int intValue = item - offset;
                buffer[intValue] = true;
            }
            var intIndex = 0;
            foreach (bool item in buffer)
            {
                if (item) yield return intIndex + offset;
                intIndex++;
            }
        }
    }
}
