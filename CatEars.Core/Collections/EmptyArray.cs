﻿using System.Collections;

namespace CatEars.Core.Collections
{
    /// <summary>
    /// 数组功能扩展
    /// </summary>
    public static partial class EmptyArray
    {
        /// <summary>
        /// 空 ArrayList
        /// </summary>
        public static ArrayList EmptyArrayList = ArrayList.ReadOnly(new ArrayList());
    }

    /// <summary>
    /// 数组功能扩展(CArray中提供基础类型的空数组【如：CArray.EmptyStringArray】，请优先使用)
    /// </summary>
    public static class EmptyArray<T>
    {
        /// <summary>
        /// 空数组
        /// </summary>
        public static T[] Array = new T[0];
    }
}
