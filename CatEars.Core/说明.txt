﻿
【命名规则】
扩展静态类以Ex开头，如：ICollection扩展类型为ExICollection、Path扩展类型为ExPath

【其它】
所有引用类型参数默认可以传入null，除非在注释中标记 【NotNull】

【目录】
CodeSegment：存放常用代码段，可以不参与编译
Extension：大量扩展方法，命名空间与.net框架保持一致
UIExtension：控件相关的扩展方法，命名空间与.net框架保持一致