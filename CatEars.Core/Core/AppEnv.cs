﻿using System;
using System.IO;
using System.Reflection;
using CatEars.Core.Console;
using CatEars.Core.Logs;

namespace CatEars.Core
{
    /// <summary>
    /// 应用程序范围内的环境参数
    /// </summary>
    public static class AppEnv
    {
        /// <summary>
        /// 静态构造方法
        /// </summary>
        static AppEnv()
        {
            InitAppPath();
            
            #region 根据运行的架构是32位还是64位设置引用组件文件夹
            var appSetupInfo = AppDomain.CurrentDomain.SetupInformation;
            string strPrivateBinPath = appSetupInfo.PrivateBinPath;
            if (string.IsNullOrWhiteSpace(strPrivateBinPath))
            {
                appSetupInfo.PrivateBinPath = Environment.Is64BitProcess ? "x64;" : "x86;";
            }
            else
            {
                appSetupInfo.PrivateBinPath = (Environment.Is64BitProcess ? "x64;" : "x86;") + appSetupInfo.PrivateBinPath;
            }
            #endregion
        }

        /// <summary>
        /// AppEnv的初始化方法
        /// </summary>
        public static void Init()
        {
            //什么都不做，但是会执行 静态构造函数
        }

        /// <summary>
        /// AppEnv的初始化方法
        /// </summary>
        public static void InitConsole()
        {
            Init();
            Log = new ColorConsoleLog(ColorConsoleEx.WithTime);
        }

        #region 应用程序路径

        /// <summary>
        /// 可执行文件路径
        /// </summary>
        public static string ExeFilePath { get; private set; }
        /// <summary>
        /// 可执行文件所在文件夹
        /// </summary>
        public static string ExeDirPath { get; private set; }

        /// <summary>
        /// 当前程序的工作目录 Environment.CurrentDirectory
        /// </summary>
        public static string CurrentDirectory
        {
            get
            {
                return ExPath.ToFullDirectoryPath(Environment.CurrentDirectory);
            }
            set
            {
                Environment.CurrentDirectory = value;
            }
        }

        /// <summary>
        /// 初始化程序相关路径
        /// </summary>
        static void InitAppPath()
        {
            string executableFilePath = Assembly.GetEntryAssembly().Location;
            //string executableFilePath = Process.GetCurrentProcess().MainModule.FileName;
            FileInfo fileInfo = new FileInfo(executableFilePath);
            ExeFilePath = fileInfo.GetFullFilePath();
            ExeDirPath = fileInfo.GetFullDirectoryPath();
        }
        #endregion

        /// <summary>
        /// 默认实例
        /// </summary>
        public static Random Random { get; } = new Random();

        static ILog _instance = new LogBase();
        /// <summary>
        /// 单例日志对象
        /// </summary>
        public static ILog Log
        {
            get { return _instance; }
            set
            {
                if (_instance != value)
                {
                    if (value == null) _instance = EmptyLog.Instance;
                    _instance = value;
                }
            }
        }
    }
}
