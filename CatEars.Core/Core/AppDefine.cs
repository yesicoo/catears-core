﻿using System.Text;

namespace CatEars.Core
{
    /// <summary>
    /// 应用程序范围内的一些定义
    /// </summary>
    public static class AppDefine
    {
        /// <summary>
        /// 换行字符串
        /// </summary>
        public static string NewLine { get; set; } = "\r\n";

        /// <summary>
        /// 默认文本编码
        /// </summary>
        public static Encoding Encoding { get; set; } = Encoding.UTF8;

        #region 时间相关

        /// <summary>
        /// yyyy_MM_dd_HH_mm_ss_fff
        /// </summary>
        public static string DateTimeFileLongFormat { get; set; } = "yyyy_MM_dd_HH_mm_ss_fff";
        /// <summary>
        /// yyyyMMdd_HHmmss_fff
        /// </summary>
        public static string DateTimeFileFormat { get; set; } = "yyyyMMdd_HHmmss_fff";
        /// <summary>
        /// yyyy-MM-dd HH:mm:ss
        /// </summary>
        public static string DateTimeFormat { get; set; } = "yyyy-MM-dd HH:mm:ss";
        /// <summary>
        /// yyyy-MM-dd
        /// </summary>
        public static string DateFormat { get; set; } = "yyyy-MM-dd";
        /// <summary>
        /// HH:mm:ss
        /// </summary>
        public static string TimeFormat { get; set; } = "HH:mm:ss";

        #endregion
    }
}
