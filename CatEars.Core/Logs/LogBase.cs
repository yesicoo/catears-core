﻿
namespace CatEars.Core.Logs
{
    /// <summary>
    /// 通用日志基础类
    /// </summary>
    public partial class LogBase : ILog
    {
        //在T4文件中实现接口

        /// <summary>
        /// 
        /// </summary>
        public LogBase() : this(null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        public LogBase(ILog parent)
        {
            if (parent == null) ParentLog = EmptyLog.Instance;
            else ParentLog = parent;
        }

        /// <summary>
        /// 组合一个父级日志
        /// </summary>
        public ILog ParentLog { get; }
    }
}
