﻿
namespace CatEars.Core.Logs
{
    /// <summary>
    /// 不实现任何内容
    /// </summary>
    public partial class EmptyLog : ILog
    {
        //在T4文件中实现接口
        /// <summary>
        /// 单例
        /// </summary>
        public static readonly EmptyLog Instance = new EmptyLog();

    }
}
