﻿using System;
using CatEars.Core.Events;
using CatEars.Core.Exceptions;

namespace CatEars.Core.Logs
{
#pragma warning disable 1591
    public partial interface ILog
    {
        event CustomEventHandler<string> OnDebug;
        void Debug(string msg);
        void Debug(string msg, params string[] args);
        void Debug(Exception exc, string msg = null);
        void Debug(Exception exc, string msg, params string[] args);

        event CustomEventHandler<string> OnInfo;
        void Info(string msg);
        void Info(string msg, params string[] args);
        void Info(Exception exc, string msg = null);
        void Info(Exception exc, string msg, params string[] args);

        event CustomEventHandler<string> OnWarn;
        void Warn(string msg);
        void Warn(string msg, params string[] args);
        void Warn(Exception exc, string msg = null);
        void Warn(Exception exc, string msg, params string[] args);

        event CustomEventHandler<string> OnError;
        void Error(string msg);
        void Error(string msg, params string[] args);
        void Error(Exception exc, string msg = null);
        void Error(Exception exc, string msg, params string[] args);

        event CustomEventHandler<string> OnFatal;
        void Fatal(string msg);
        void Fatal(string msg, params string[] args);
        void Fatal(Exception exc, string msg = null);
        void Fatal(Exception exc, string msg, params string[] args);

        event CustomEventHandler<string> OnSuccess;
        void Success(string msg);
        void Success(string msg, params string[] args);
        void Success(Exception exc, string msg = null);
        void Success(Exception exc, string msg, params string[] args);

    }
#pragma warning disable 1591
    public partial class EmptyLog
    {
#pragma warning disable 0067
        public event CustomEventHandler<string> OnDebug;
        public void Debug(string msg) { }
        public void Debug(string msg, params string[] args) { }
        public void Debug(Exception exc, string msg = null) { }
        public void Debug(Exception exc, string msg, params string[] args) { }

#pragma warning disable 0067
        public event CustomEventHandler<string> OnInfo;
        public void Info(string msg) { }
        public void Info(string msg, params string[] args) { }
        public void Info(Exception exc, string msg = null) { }
        public void Info(Exception exc, string msg, params string[] args) { }

#pragma warning disable 0067
        public event CustomEventHandler<string> OnWarn;
        public void Warn(string msg) { }
        public void Warn(string msg, params string[] args) { }
        public void Warn(Exception exc, string msg = null) { }
        public void Warn(Exception exc, string msg, params string[] args) { }

#pragma warning disable 0067
        public event CustomEventHandler<string> OnError;
        public void Error(string msg) { }
        public void Error(string msg, params string[] args) { }
        public void Error(Exception exc, string msg = null) { }
        public void Error(Exception exc, string msg, params string[] args) { }

#pragma warning disable 0067
        public event CustomEventHandler<string> OnFatal;
        public void Fatal(string msg) { }
        public void Fatal(string msg, params string[] args) { }
        public void Fatal(Exception exc, string msg = null) { }
        public void Fatal(Exception exc, string msg, params string[] args) { }

#pragma warning disable 0067
        public event CustomEventHandler<string> OnSuccess;
        public void Success(string msg) { }
        public void Success(string msg, params string[] args) { }
        public void Success(Exception exc, string msg = null) { }
        public void Success(Exception exc, string msg, params string[] args) { }

	}
#pragma warning disable 1591
    public partial class LogBase
    {
        public event CustomEventHandler<string> OnDebug;
        public virtual void Debug(string msg)
        {
            ParentLog.Debug(msg);
            OnDebug?.Invoke(this, msg);
        }
        public virtual void Debug(string msg, params string[] args)
        {
            Debug(msg.SafeFormat(args));
        }
        public virtual void Debug(Exception exc, string msg = null)
        {
            if (string.IsNullOrEmpty(msg)) Debug(exc.ToLogString());
            else Debug('【' + msg + '】' + exc.ToLogString());
        }
        public virtual void Debug(Exception exc, string msg, params string[] args)
        {
            Debug(exc, msg.SafeFormat(args));
        }

        public event CustomEventHandler<string> OnInfo;
        public virtual void Info(string msg)
        {
            ParentLog.Info(msg);
            OnInfo?.Invoke(this, msg);
        }
        public virtual void Info(string msg, params string[] args)
        {
            Info(msg.SafeFormat(args));
        }
        public virtual void Info(Exception exc, string msg = null)
        {
            if (string.IsNullOrEmpty(msg)) Info(exc.ToLogString());
            else Info('【' + msg + '】' + exc.ToLogString());
        }
        public virtual void Info(Exception exc, string msg, params string[] args)
        {
            Info(exc, msg.SafeFormat(args));
        }

        public event CustomEventHandler<string> OnWarn;
        public virtual void Warn(string msg)
        {
            ParentLog.Warn(msg);
            OnWarn?.Invoke(this, msg);
        }
        public virtual void Warn(string msg, params string[] args)
        {
            Warn(msg.SafeFormat(args));
        }
        public virtual void Warn(Exception exc, string msg = null)
        {
            if (string.IsNullOrEmpty(msg)) Warn(exc.ToLogString());
            else Warn('【' + msg + '】' + exc.ToLogString());
        }
        public virtual void Warn(Exception exc, string msg, params string[] args)
        {
            Warn(exc, msg.SafeFormat(args));
        }

        public event CustomEventHandler<string> OnError;
        public virtual void Error(string msg)
        {
            ParentLog.Error(msg);
            OnError?.Invoke(this, msg);
        }
        public virtual void Error(string msg, params string[] args)
        {
            Error(msg.SafeFormat(args));
        }
        public virtual void Error(Exception exc, string msg = null)
        {
            if (string.IsNullOrEmpty(msg)) Error(exc.ToLogString());
            else Error('【' + msg + '】' + exc.ToLogString());
        }
        public virtual void Error(Exception exc, string msg, params string[] args)
        {
            Error(exc, msg.SafeFormat(args));
        }

        public event CustomEventHandler<string> OnFatal;
        public virtual void Fatal(string msg)
        {
            ParentLog.Fatal(msg);
            OnFatal?.Invoke(this, msg);
        }
        public virtual void Fatal(string msg, params string[] args)
        {
            Fatal(msg.SafeFormat(args));
        }
        public virtual void Fatal(Exception exc, string msg = null)
        {
            if (string.IsNullOrEmpty(msg)) Fatal(exc.ToLogString());
            else Fatal('【' + msg + '】' + exc.ToLogString());
        }
        public virtual void Fatal(Exception exc, string msg, params string[] args)
        {
            Fatal(exc, msg.SafeFormat(args));
        }

        public event CustomEventHandler<string> OnSuccess;
        public virtual void Success(string msg)
        {
            ParentLog.Success(msg);
            OnSuccess?.Invoke(this, msg);
        }
        public virtual void Success(string msg, params string[] args)
        {
            Success(msg.SafeFormat(args));
        }
        public virtual void Success(Exception exc, string msg = null)
        {
            if (string.IsNullOrEmpty(msg)) Success(exc.ToLogString());
            else Success('【' + msg + '】' + exc.ToLogString());
        }
        public virtual void Success(Exception exc, string msg, params string[] args)
        {
            Success(exc, msg.SafeFormat(args));
        }

	}
}
