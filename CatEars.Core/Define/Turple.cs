﻿
//namespace CatEars.CDefine
//{
//    /// <summary>
//    /// Turple静态方法
//    /// </summary>
//    public static class Turple
//    {
//        /// <summary>
//        /// 创建Turple对象
//        /// </summary>
//        /// <typeparam name="T1"></typeparam>
//        /// <typeparam name="T2"></typeparam>
//        /// <param name="t1"></param>
//        /// <param name="t2"></param>
//        /// <returns></returns>
//        public static Turple<T1, T2> Create<T1, T2>(T1 t1, T2 t2)
//        {
//            return new Turple<T1, T2>()
//            {
//                O1 = t1,
//                O2 = t2,
//            };
//        }

//        /// <summary>
//        /// 创建Turple对象
//        /// </summary>
//        /// <typeparam name="T1"></typeparam>
//        /// <typeparam name="T2"></typeparam>
//        /// <typeparam name="T3"></typeparam>
//        /// <param name="t1"></param>
//        /// <param name="t2"></param>
//        /// <param name="t3"></param>
//        /// <returns></returns>
//        public static Turple<T1, T2, T3> Create<T1, T2, T3>(T1 t1, T2 t2, T3 t3)
//        {
//            return new Turple<T1, T2, T3>()
//            {
//                O1 = t1,
//                O2 = t2,
//                O3 = t3,
//            };
//        }
//        /// <summary>
//        /// 创建Turple对象
//        /// </summary>
//        /// <typeparam name="T1"></typeparam>
//        /// <typeparam name="T2"></typeparam>
//        /// <typeparam name="T3"></typeparam>
//        /// <typeparam name="T4"></typeparam>
//        /// <param name="t1"></param>
//        /// <param name="t2"></param>
//        /// <param name="t3"></param>
//        /// <param name="t4"></param>
//        /// <returns></returns>
//        public static Turple<T1, T2, T3, T4> Create<T1, T2, T3, T4>(
//            T1 t1, T2 t2, T3 t3, T4 t4)
//        {
//            return new Turple<T1, T2, T3, T4>()
//            {
//                O1 = t1,
//                O2 = t2,
//                O3 = t3,
//                O4 = t4,
//            };
//        }
//        /// <summary>
//        /// 创建Turple对象
//        /// </summary>
//        /// <typeparam name="T1"></typeparam>
//        /// <typeparam name="T2"></typeparam>
//        /// <typeparam name="T3"></typeparam>
//        /// <typeparam name="T4"></typeparam>
//        /// <typeparam name="T5"></typeparam>
//        /// <param name="t1"></param>
//        /// <param name="t2"></param>
//        /// <param name="t3"></param>
//        /// <param name="t4"></param>
//        /// <param name="t5"></param>
//        /// <returns></returns>
//        public static Turple<T1, T2, T3, T4, T5> Create<T1, T2, T3, T4, T5>(
//            T1 t1, T2 t2, T3 t3, T4 t4, T5 t5)
//        {
//            return new Turple<T1, T2, T3, T4, T5>()
//            {
//                O1 = t1,
//                O2 = t2,
//                O3 = t3,
//                O4 = t4,
//                O5 = t5,
//            };
//        }
//        /// <summary>
//        /// 创建Turple对象
//        /// </summary>
//        /// <typeparam name="T1"></typeparam>
//        /// <typeparam name="T2"></typeparam>
//        /// <typeparam name="T3"></typeparam>
//        /// <typeparam name="T4"></typeparam>
//        /// <typeparam name="T5"></typeparam>
//        /// <typeparam name="T6"></typeparam>
//        /// <param name="t1"></param>
//        /// <param name="t2"></param>
//        /// <param name="t3"></param>
//        /// <param name="t4"></param>
//        /// <param name="t5"></param>
//        /// <param name="t6"></param>
//        /// <returns></returns>
//        public static Turple<T1, T2, T3, T4, T5, T6> Create<T1, T2, T3, T4, T5, T6>(
//            T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6)
//        {
//            return new Turple<T1, T2, T3, T4, T5, T6>()
//            {
//                O1 = t1,
//                O2 = t2,
//                O3 = t3,
//                O4 = t4,
//                O5 = t5,
//                O6 = t6,
//            };
//        }
//    }

//    /// <summary>
//    /// 组合对象
//    /// </summary>
//    /// <typeparam name="T1">对象1类型</typeparam>
//    /// <typeparam name="T2">对象2类型</typeparam>
//    public class Turple<T1, T2>
//    {
//        /// <summary>
//        /// 实体T1
//        /// </summary>
//        public T1 O1
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// 实体T2
//        /// </summary>
//        public T2 O2
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// 重写ToString()
//        /// </summary>
//        /// <returns>字符串</returns>
//        public override string ToString()
//        {
//            return string.Format(
//                "O1:{0} O2:{1}",
//                O1 == null ? "null" : O1.ToString(),
//                O2 == null ? "null" : O2.ToString());
//        }
//    }

//    /// <summary>
//    /// 组合对象
//    /// </summary>
//    /// <typeparam name="T1">对象1类型</typeparam>
//    /// <typeparam name="T2">对象2类型</typeparam>
//    /// <typeparam name="T3">对象3类型</typeparam>
//    public class Turple<T1, T2, T3> : Turple<T1, T2>
//    {
//        /// <summary>
//        /// 实体T3
//        /// </summary>
//        public T3 O3
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// 重写ToString()
//        /// </summary>
//        /// <returns>字符串</returns>
//        public override string ToString()
//        {
//            return base.ToString()
//                + " O3:"
//                + O3 == null ? "null" : O3.ToString();
//        }
//    }

//    /// <summary>
//    /// 组合对象
//    /// </summary>
//    /// <typeparam name="T1">对象1类型</typeparam>
//    /// <typeparam name="T2">对象2类型</typeparam>
//    /// <typeparam name="T3">对象3类型</typeparam>
//    /// <typeparam name="T4">对象4类型</typeparam>
//    public class Turple<T1, T2, T3, T4> : Turple<T1, T2, T3>
//    {
//        /// <summary>
//        /// 实体T4
//        /// </summary>
//        public T4 O4
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// 重写ToString()
//        /// </summary>
//        /// <returns>字符串</returns>
//        public override string ToString()
//        {
//            return base.ToString()
//                + " O4:"
//                + O4 == null ? "null" : O4.ToString();
//        }
//    }

//    /// <summary>
//    /// 组合对象
//    /// </summary>
//    /// <typeparam name="T1">对象1类型</typeparam>
//    /// <typeparam name="T2">对象2类型</typeparam>
//    /// <typeparam name="T3">对象3类型</typeparam>
//    /// <typeparam name="T4">对象4类型</typeparam>
//    /// <typeparam name="T5">对象5类型</typeparam>
//    public class Turple<T1, T2, T3, T4, T5> : Turple<T1, T2, T3, T4>
//    {
//        /// <summary>
//        /// 实体T5
//        /// </summary>
//        public T5 O5
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// 重写ToString()
//        /// </summary>
//        /// <returns>字符串</returns>
//        public override string ToString()
//        {
//            return base.ToString()
//                + " O5:"
//                + O5 == null ? "null" : O5.ToString();
//        }
//    }

//    /// <summary>
//    /// 组合对象
//    /// </summary>
//    /// <typeparam name="T1">对象1类型</typeparam>
//    /// <typeparam name="T2">对象2类型</typeparam>
//    /// <typeparam name="T3">对象3类型</typeparam>
//    /// <typeparam name="T4">对象4类型</typeparam>
//    /// <typeparam name="T5">对象5类型</typeparam>
//    /// <typeparam name="T6">对象6类型</typeparam>
//    public class Turple<T1, T2, T3, T4, T5, T6> : Turple<T1, T2, T3, T4, T5>
//    {
//        /// <summary>
//        /// 实体T6
//        /// </summary>
//        public T6 O6
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// 重写ToString()
//        /// </summary>
//        /// <returns>字符串</returns>
//        public override string ToString()
//        {
//            return base.ToString()
//                + " O6:"
//                + O6 == null ? "null" : O6.ToString();
//        }
//    }
//}
