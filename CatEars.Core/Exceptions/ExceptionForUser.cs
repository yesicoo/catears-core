﻿using System;

namespace CatEars.Core.Exceptions
{
    /// <summary>
    /// 提示给用户的异常信息
    /// </summary>
    public class ExceptionForUser : Exception
    {
        /// <summary>
        /// 异常标识ID
        /// </summary>
        public int ID { get; }
        /// <summary>
        /// 提示给用户的异常信息
        /// </summary>
        /// <param name="id">异常标识ID</param>
        /// <param name="msg">异常消息</param>
        /// <param name="innerExc">异常内容</param>
        public ExceptionForUser(int id, string msg, Exception innerExc = null)
            : base(msg, innerExc)
        {
            ID = id;
        }
    }
}
