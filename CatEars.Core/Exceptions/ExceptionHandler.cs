﻿using System;

namespace CatEars.Core.Exceptions
{
    /// <summary>
    /// 异常处理器
    /// </summary>
    public static class ExceptionHandler
    {
        /// <summary>
        /// 转换为日志输出格式
        /// </summary>
        /// <param name="exc">【NotNull】</param>
        /// <returns></returns>
        public static string ToLogString(this Exception exc)
        {
            return exc.ToString();
        }
        /// <summary>
        /// 转换为用户提示格式
        /// </summary>
        /// <param name="exc">【NotNull】</param>
        /// <returns></returns>
        public static string ToDisplayString(this Exception exc)
        {
            return exc.Message;
        }
    }
}
