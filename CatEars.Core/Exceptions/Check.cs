﻿using System;

namespace CatEars.Core.Exceptions
{
    /// <summary>
    /// 条件检查
    /// </summary>
    public static class Check
    {
        /// <summary>
        /// 断言
        /// </summary>
        /// <param name="condition">condition</param>
        /// <param name="message">message</param>
        public static void Assert(bool condition, string message)
        {
            if (!condition)
            {
                throw new Exception(message);
            }
        }

        /// <summary>
        /// 断言
        /// </summary>
        /// <param name="condition">condition</param>
        public static void Assert<T>(bool condition) where T : Exception
        {
            if (!condition)
            {
                throw Activator.CreateInstance<T>();
            }
        }

        /// <summary>
        /// 断言
        /// </summary>
        /// <param name="condition">condition</param>
        /// <param name="args">args</param>
        public static void Assert<T>(bool condition, params object[] args)
            where T : Exception
        {
            if (!condition)
            {
                throw (Exception)Activator.CreateInstance(typeof(T), args);
            }
        }

        /// <summary>
        /// 断言非空
        /// </summary>
        /// <param name="obj">对象</param>
        public static void AssertArguNotNull(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException();
            }
        }

        /// <summary>
        /// 断言非空
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="strParamName">参数名</param>
        public static void AssertArguNotNull(object obj, string strParamName)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(strParamName);
            }
        }

        /// <summary>
        /// 断言非空
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="strParamName">参数名</param>
        /// <param name="message">异常消息</param>
        public static void AssertArguNotNull(
            object obj,
            string strParamName,
            string message)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(strParamName, message);
            }
        }

        /// <summary>
        /// 断言非空
        /// </summary>
        /// <param name="strValue">字符串</param>
        /// <param name="strParamName">参数名</param>
        public static void AssertStrNotEmpry(
            string strValue,
            string strParamName)
        {
            if (string.IsNullOrEmpty(strValue))
            {
                throw new ArgumentException(strParamName, "字符串不能为空串");
            }
        }
    }
}
