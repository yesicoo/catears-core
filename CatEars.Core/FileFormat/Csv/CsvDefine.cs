﻿
using System;
using System.Diagnostics;
using System.Text;

namespace CatEars.Core.FileFormat.Csv
{
    /// <summary>
    /// Csv格式定义
    /// </summary>
    [DebuggerDisplay("Encoding={Encoding} FristRowIsHeader={FristRowIsHeader} WriteBorderForAllCell={WriteBorderForAllCell} SplitChar={SplitChar} BorderChar={BorderChar} EscapeChar={EscapeChar} NewLine={NewLine}")]
    public class CsvDefine
    {
        /// <summary>
        /// 默认编码名称 默认UTF-8
        /// </summary>
        public static string DefaultEncodingName { get; private set; }

        /// <summary>
        /// 默认参数
        /// </summary>
        public static CsvDefine Default { get; private set; }
        /// <summary>
        /// 默认参数
        /// </summary>
        public static CsvDefine DefaultWithBorder { get; private set; }

        /// <summary>
        /// 静态初始化
        /// </summary>
        static CsvDefine()
        {
            InitCsvDefine("UTF-8");
        }
        /// <summary>
        /// 初始化CsvDefine默认参数
        /// </summary>
        /// <param name="defaultEncodingName"></param>
        public static void InitCsvDefine(string defaultEncodingName)
        {
            DefaultEncodingName = defaultEncodingName;
            Default = new CsvDefine();

            DefaultWithBorder = Default.Clone();
            DefaultWithBorder.WriteBorderForAllCell = true;
        }

        /// <summary>
        /// 复制对象
        /// </summary>
        /// <returns></returns>
        public CsvDefine Clone()
        {
            return new CsvDefine()
            {
                Encoding = this.Encoding,
                FristRowIsHeader = this.FristRowIsHeader,
                SplitChar = this.SplitChar,
                BorderChar = this.BorderChar,
                EscapeChar = this.EscapeChar,
                ReplaceNewLineAtWrite = this.ReplaceNewLineAtWrite,
                IgnoreEmptyRowAtRead = this.IgnoreEmptyRowAtRead,
                NewLine = this.NewLine,
                WriteBorderForAllCell = this.WriteBorderForAllCell,
                AutoFlushRowCount = this.AutoFlushRowCount,
            };
        }

        /// <summary>
        /// 初始化CsvParam对象(若正常格式，请使用Clone创建对象)
        /// </summary>
        public CsvDefine()
        {
            Encoding = DefaultEncodingName;
            FristRowIsHeader = true;
            SplitChar = ',';
            BorderChar = '"';
            EscapeChar = '"';
            ReplaceNewLineAtWrite = null;
            IgnoreEmptyRowAtRead = true;
            NewLine = "\r\n";
            WriteBorderForAllCell = false;
            AutoFlushRowCount = 1024;
        }

        /// <summary>
        /// [读/写]编码格式对应的Encoding对象
        /// </summary>
        private Encoding m_encodingOjbect;
        /// <summary>
        /// 获取Encoding对象
        /// </summary>
        public Encoding GetEncoding() { return m_encodingOjbect; }
        /// <summary>
        /// 设置Encoding对象
        /// </summary>
        public void SetEncoding(Encoding value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("EncodingOjbect", "EncodingOjbect不能为null");
            }
            m_encodingOjbect = value;
            m_strEncodingName = m_encodingOjbect.WebName;
        }

        /// <summary>
        /// 编码名称
        /// </summary>
        private string m_strEncodingName;
        /// <summary>
        /// [读/写][CsvDefine.DefaultEncodingName]编码名称
        /// </summary>
        public string Encoding
        {
            get { return m_strEncodingName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("Encoding", "Encoding不能为null");
                }
                var enc = System.Text.Encoding.GetEncoding(value);
                if (enc == null)
                {
                    throw new NullReferenceException(
                        string.Format("“{0}”不是有效的编码名称", value));
                }
                m_strEncodingName = value;
                m_encodingOjbect = enc;
            }
        }

        /// <summary>
        /// [读/写][true]首行为标题
        /// </summary>
        public bool FristRowIsHeader { get; set; }

        /// <summary>
        /// [读/写][,]分隔符
        /// </summary>
        public char SplitChar { get; set; }

        /// <summary>
        /// [读/写]["]单元格边界字符
        /// </summary>
        public char BorderChar { get; set; }

        /// <summary>
        /// [读/写]["]转义字符
        /// </summary>
        public char EscapeChar { get; set; }

        /// <summary>
        /// [写][null]写入时将换行替换为设置的字符串
        /// </summary>
        public string ReplaceNewLineAtWrite { get; set; }

        /// <summary>
        /// [读][true]读取csv行的时候 忽略空行（存在问题:在csv列数为1，且空行为有效单元格时会忽略这样的行）
        /// </summary>
        public bool IgnoreEmptyRowAtRead { get; set; }

        /// <summary>
        /// [写(读的时候直接忽略\r)]默认\r\n,可以改为\n
        /// </summary>
        public string NewLine { get; set; }

        /// <summary>
        /// [写][false]每个单元格都添加边界字符
        /// </summary>
        public bool WriteBorderForAllCell { get; set; }

        /// <summary>
        /// [写][1024]每AutoFlushRowCount行自动Flush
        /// </summary>
        public int AutoFlushRowCount { get; set; }
    }
}
