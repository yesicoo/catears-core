﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using CatEars.Core.Collections;
using CatEars.Core.Data.KvParam;

namespace CatEars.Core.FileFormat.Ini
{
    /// <summary>
    /// Ini文件中的参数组
    /// </summary>
    [DebuggerDisplay("Name=\"{Name}\" Count={Count} TextCount={_allRows.Count}")]
    public class IniSection : ParamBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 初始化Section
        /// </summary>
        /// <param name="strName"></param>
        public IniSection(string strName)
        {
            Name = strName;
        }

        /// <summary>
        /// 所有行
        /// </summary>
        public ICollection<string> AllRows { get { return _allRows; } }
        //操作以下2个集合的时候锁一下_innerDict
        /// <summary>
        /// 来自原文件中的行
        /// </summary>
        List<string> _allRows = new List<string>();
        /// <summary>
        /// 内部key-value
        /// </summary>
        DictEx<string, string> _innerDict = new DictEx<string, string>();

        /// <summary>
        /// 数量
        /// </summary>
        public override int Count { get { return _innerDict.Count; } }

        /// <summary>
        /// 获取已有的Key
        /// </summary>
        public override ICollection<string> Keys
        {
            get { return _innerDict.Keys; }
        }

        #region 抽象方法
        /// <summary>
        /// 清空
        /// </summary>
        public override void Clear()
        {
            lock (_innerDict)
            {
                _innerDict.Clear();
            }
        }

        /// <summary>
        /// 取值
        /// </summary>
        /// <param name="strKey">key</param>
        /// <param name="strDefault">默认值</param>
        /// <returns>value</returns>
        protected override string XGet(string strKey, string strDefault = null)
        {
            //改成转义的就没有换行的限制了
            return _innerDict.GetOrDefault(strKey, strDefault);
        }

        /// <summary>
        /// 设置值
        /// </summary>
        /// <param name="strKey">key</param>
        /// <param name="strValue">value</param>
        protected override void XSet(string strKey, string strValue)
        {
            //改成转义的就没有换行的限制了
            string strValue2 = strValue ?? "";
            if (strValue2.Contains("\r") || strValue2.Contains("\n"))
            {
                throw new ArgumentException("value中不能含有换行", "value");
            }
            lock (_innerDict)
            {
                bool blnReplace = _innerDict.AddOrReplace(strKey, strValue2);
                if (!blnReplace)
                {
                    _allRows.Add("[" + strKey + "]");
                }
            }
        }
        #endregion

        #region 文档注释相关
        /// <summary>
        /// 添加注释文档
        /// </summary>
        /// <param name="strText">文档</param>
        public void AddText(string strText)
        {
            string strText0 = strText ?? "";
            //直接添加 不自动添加注释符 写入文件时会自动添加
            //if (!string.IsNullOrWhiteSpace(strText0) && !strText0.StartsWith(";"))
            //{
            //    strText0 = ";" + strText0;
            //}
            lock (_innerDict)
            {
                _allRows.Add(strText0);
            }
        }

        /// <summary>
        /// 删除所有注释文档
        /// </summary>
        public void ClearText()
        {
            lock (_innerDict)
            {
                for (int i = _allRows.Count - 1; i >= 0; i--)
                {
                    string strText = _allRows[i];
                    if (strText.StartsWith(";"))
                    {
                        _allRows.RemoveAt(i);
                    }
                }
                //删除多余空行
                for (int i = _allRows.Count - 2; i >= 0; i--)
                {
                    string strText1 = _allRows[i];
                    string strText2 = _allRows[i + 1];
                    if (strText1.Length == 0 && strText2.Length == 0)
                    {
                        _allRows.RemoveAt(i + 1);
                    }
                }
            }
        }

        /// <summary>
        /// 删除所有注释文档和参数
        /// </summary>
        public void ClearAll()
        {
            lock (_innerDict)
            {
                Clear();
                ClearText();
            }
        }
        #endregion
    }
}
