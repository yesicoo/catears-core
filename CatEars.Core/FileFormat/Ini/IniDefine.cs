﻿
using System;
using System.Diagnostics;
using System.Text;

namespace CatEars.Core.FileFormat.Ini
{
    /// <summary>
    /// Ini格式定义
    /// </summary>
    [DebuggerDisplay("Encoding={Encoding} NewLine={NewLine}")]
    public class IniDefine
    {
        /// <summary>
        /// 默认编码名称 默认UTF-8
        /// </summary>
        public static string DefaultEncodingName { get; private set; }
        /// <summary>
        /// 默认参数
        /// </summary>
        public static IniDefine Default { get; private set; }

        /// <summary>
        /// 静态初始化
        /// </summary>
        static IniDefine()
        {
            InitIniDefine("UTF-8");
        }
        /// <summary>
        /// 初始化IniDefine默认参数
        /// </summary>
        /// <param name="defaultEncodingName"></param>
        public static void InitIniDefine(string defaultEncodingName)
        {
            DefaultEncodingName = defaultEncodingName;
            Default = new IniDefine();
        }

        /// <summary>
        /// 复制对象
        /// </summary>
        /// <returns></returns>
        public IniDefine Clone()
        {
            return new IniDefine()
            {
                Encoding = this.Encoding,
                NewLine = this.NewLine,
                IgnoreRowStart = this.IgnoreRowStart,
            };
        }

        /// <summary>
        /// 初始化CsvParam对象(若正常格式，请使用Clone创建对象)
        /// </summary>
        public IniDefine()
        {
            Encoding = DefaultEncodingName;
            NewLine = "\r\n";
            IgnoreRowStart = new char[] { ';', '#' };
        }

        /// <summary>
        /// [读/写]编码格式对应的Encoding对象
        /// </summary>
        private Encoding m_encodingOjbect;
        /// <summary>
        /// 获取Encoding对象
        /// </summary>
        public Encoding GetEncoding() { return m_encodingOjbect; }
        /// <summary>
        /// 设置Encoding对象
        /// </summary>
        public void SetEncoding(Encoding value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("EncodingOjbect", "EncodingOjbect不能为null");
            }
            m_encodingOjbect = value;
            m_strEncodingName = m_encodingOjbect.WebName;
        }
        /// <summary>
        /// 编码名称
        /// </summary>
        private string m_strEncodingName;
        /// <summary>
        /// [读/写][DefaultEncodingName]编码名称
        /// </summary>
        public string Encoding
        {
            get { return m_strEncodingName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("Encoding", "Encoding不能为null");
                }
                var enc = System.Text.Encoding.GetEncoding(value);
                if (enc == null)
                {
                    throw new NullReferenceException(
                        string.Format("“{0}”不是有效的编码名称", value));
                }
                m_strEncodingName = value;
                m_encodingOjbect = enc;
            }
        }

        /// <summary>
        /// [写(读的时候直接忽略\r)]默认\r\n,可以改为\n
        /// </summary>
        public string NewLine { get; set; }

        /// <summary>
        /// 以指定字符开头的作为注释忽略(至少必须有一项)
        /// </summary>
        public char[] IgnoreRowStart { get; set; }
    }
}
