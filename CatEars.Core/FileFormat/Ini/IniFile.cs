﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using CatEars.Core.Collections;

namespace CatEars.Core.FileFormat.Ini
{
    /// <summary>
    /// Ini文件实例
    /// </summary>
    [DebuggerDisplay("Count={_sections.Count}")]
    public class IniFile
    {
        /// <summary>
        /// 读取时的ini文件格式【文件读取后为了记录编码，允许set】
        /// </summary>
        public IniDefine IniDefine { get; internal set; }

        /// <summary>
        /// 创建一个空的IniEntity
        /// </summary>
        public IniFile()
        {
            //添加一个默认的Section
            this.AddSection(null);
        }

        /// <summary>
        /// 从文件初始化一个IniEntity，并指定编码格式
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <param name="define"></param>
        public IniFile(string strFilePath, IniDefine define = null)
            : this()
        {
            IniFormater.LoadFromFile(this, strFilePath, define);
        }

        /// <summary>
        /// 保存配置到文件
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <param name="define"></param>
        public void Save(string strFilePath, IniDefine define = null)
        {
            if (define == null) define = IniDefine;
            IniFormater.SaveToFile(this, strFilePath, define);
        }

        /// <summary>
        /// 同步对象
        /// </summary>
        public object Sync { get { return _sections; } }

        /// <summary>
        /// Ini文件中的Section
        /// </summary>
        DictEx<string, IniSection> _sections = new DictEx<string, IniSection>();

        /// <summary>
        /// 获取所有Section的名称
        /// </summary>
        public ICollection<string> SectionNames { get { return _sections.Keys; } }

        /// <summary>
        /// Section数量
        /// </summary>
        public int SectionCount { get { return _sections.Count; } }

        /// <summary>
        /// 默认的Section
        /// </summary>
        public IniSection DefaultSection
        {
            //空值表示默认的
            get { return this[null]; }
        }

        /// <summary>
        /// 获取一个Section
        /// </summary>
        /// <param name="strSectionName">SectionName</param>
        /// <returns>IniSection</returns>
        public IniSection this[string strSectionName]
        {
            get
            {
                string strSectionName0 = (strSectionName ?? "").Trim();
                return _sections.GetOrDefault(strSectionName0);
            }
        }

        /// <summary>
        /// 获取一个Group用于读参数
        /// </summary>
        /// <param name="strSectionName">strSectionName</param>
        /// <returns>IniSection</returns>
        public IniSection Gr(string strSectionName)
        {
            string strSectionName0 = (strSectionName ?? "").Trim();
            var p = this[strSectionName0];
            if (p == null)
            {
                //创建一个空的 不添加到字典中
                p = new IniSection(strSectionName0);
            }
            return p;
        }

        /// <summary>
        /// 获取一个Group用于写参数
        /// </summary>
        /// <param name="strSectionName">strSectionName</param>
        /// <returns>IniSection</returns>
        public IniSection Gw(string strSectionName)
        {
            lock (Sync)
            {
                string strSectionName0 = (strSectionName ?? "").Trim();
                var p = this[strSectionName0];
                if (p == null)
                {
                    p = new IniSection(strSectionName0);
                    _sections[strSectionName0] = p;
                }
                return p;
            }
        }

        /// <summary>
        /// 添加一个Section
        /// </summary>
        /// <param name="strSectionName"></param>
        /// <returns></returns>
        public IniSection AddSection(string strSectionName)
        {
            lock (Sync)
            {
                string strSectionName0 = (strSectionName ?? "").Trim();
                var sec = this[strSectionName0];
                if (sec != null)
                {
                    throw new ArgumentException(
                        string.Format("SectionName:“{0}”已存在", sec.Name), "SectionName");
                }
                sec = new IniSection(strSectionName0);
                _sections[strSectionName0] = sec;
                return sec;
            }
        }

        /// <summary>
        /// 删除一个Section
        /// </summary>
        /// <param name="strSectionName"></param>
        /// <returns></returns>
        public bool RemoveSection(string strSectionName)
        {
            string strSectionName0 = (strSectionName ?? "").Trim();
            if (string.IsNullOrEmpty(strSectionName0))
            {
                throw new NotSupportedException("默认组不允许删除");
            }
            lock (Sync)
            {
                return _sections.Remove(strSectionName0);
            }
        }
    }
}
