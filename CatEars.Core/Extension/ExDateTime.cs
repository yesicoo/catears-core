﻿using CatEars.Core;
using CatEars.Core.Define;

namespace System
{
    /// <summary>
    /// System.DateTime 扩展
    /// </summary>
    public static class ExDateTime
    {
        #region LunarDateTime

        /// <summary>
        /// 获取指定公历时间转换为农历时间
        /// </summary>
        /// <param name="dateTime">公历时间【NotNull】</param>
        /// <returns>农历时间</returns>
        public static LunarDateTime ToChinaDateTime(this DateTime dateTime)
        {
            return new LunarDateTime(dateTime);
        }

        #endregion

        #region Format
        /// <summary>
        /// 按 DateTimeFileFormat 格式化时间
        /// </summary>
        /// <param name="dateTime">【NotNull】</param>
        /// <returns></returns>
        public static string ToDateTimeFileFormat(this DateTime dateTime)
        {
            return dateTime.ToString(AppDefine.DateTimeFileFormat);
        }
        /// <summary>
        /// 按 DateTimeFileLongFormat 格式化时间
        /// </summary>
        /// <param name="dateTime">【NotNull】</param>
        /// <returns></returns>
        public static string ToDateTimeFileLongFormat(this DateTime dateTime)
        {
            return dateTime.ToString(AppDefine.DateTimeFileLongFormat);
        }

        /// <summary>
        /// 按 DateTimeFormat 格式化时间
        /// </summary>
        /// <param name="dateTime">【NotNull】</param>
        /// <returns></returns>
        public static string ToDateTimeFormat(this DateTime dateTime)
        {
            return dateTime.ToString(AppDefine.DateTimeFormat);
        }

        /// <summary>
        /// 按 DateFormat 格式化时间
        /// </summary>
        /// <param name="dateTime">【NotNull】</param>
        /// <returns></returns>
        public static string ToDateFormat(this DateTime dateTime)
        {
            return dateTime.ToString(AppDefine.DateFormat);
        }

        /// <summary>
        /// 按 TimeFormat 格式化时间
        /// </summary>
        /// <param name="dateTime">【NotNull】</param>
        /// <returns></returns>
        public static string ToTimeFormat(this DateTime dateTime)
        {
            return dateTime.ToString(AppDefine.TimeFormat);
        }
        #endregion
    }
}
