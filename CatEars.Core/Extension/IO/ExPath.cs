﻿using CatEars.Core;

namespace System.IO
{
    /// <summary>
    /// System.IO.Path 的扩展
    /// </summary>
    public static class ExPath
    {
        /// <summary>
        /// 将路径转换为完整路径，用/分隔
        /// </summary>
        /// <param name="filePath">【NotNull】文件路径</param>
        /// <returns></returns>
        public static string ToFullFilePath(this string filePath)
        {
            if (!Path.IsPathRooted(filePath))
            {
                filePath = Path.Combine(AppEnv.ExeDirPath, filePath);
            }
            string result = Path.GetFullPath(filePath).Replace("\\", "/");
            return result;
        }

        /// <summary>
        /// 将路径转换为完整路径，用/分隔
        /// </summary>
        /// <param name="dirPath">【NotNull】文件夹路径</param>
        /// <returns></returns>
        public static string ToFullDirectoryPath(this string dirPath)
        {
            string path0 = ToFullFilePath(dirPath);
            if (!path0.EndsWith("/")) path0 += "/";
            return path0;
        }

        /// <summary>
        /// 获取文件所在的文件夹路径
        /// </summary>
        /// <param name="filePath">【NotNull】文件路径</param>
        /// <returns></returns>
        public static string GetFullDirectoryPath(this string filePath)
        {
            FileInfo fi = new FileInfo(filePath);
            return ToFullDirectoryPath(fi.Directory.FullName);
        }
    }
}
