﻿
namespace System.IO
{
    /// <summary>
    /// System.IO.DirectoryInfo 的扩展
    /// </summary>
    public static class ExDirectoryInfo
    {
        /// <summary>
        /// ExPath.ToFullDirectoryPath
        /// </summary>
        /// <param name="dirInfo">【NotNull】</param>
        /// <returns></returns>
        public static string GetFullDirectoryPath(this DirectoryInfo dirInfo)
        {
            return ExPath.ToFullDirectoryPath(dirInfo.FullName);
        }
    }
}
