﻿
namespace System.IO
{
    /// <summary>
    /// System.IO.FileInfo 的扩展
    /// </summary>
    public static class ExFileInfo
    {
        /// <summary>
        /// ExPath.ToFullFilePath
        /// </summary>
        /// <param name="fileInfo">【NotNull】</param>
        /// <returns></returns>
        public static string GetFullFilePath(this FileInfo fileInfo)
        {
            return ExPath.ToFullFilePath(fileInfo.FullName);
        }

        /// <summary>
        /// ExPath.GetFullDirectoryPath
        /// </summary>
        /// <param name="fileInfo">【NotNull】</param>
        /// <returns></returns>
        public static string GetFullDirectoryPath(this FileInfo fileInfo)
        {
            return ExPath.GetFullDirectoryPath(fileInfo.FullName);
        }
    }
}
