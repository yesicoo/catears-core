﻿
namespace System.Collections
{
    /// <summary>
    /// 集合扩展方法
    /// </summary>
    public static class ExICollection
    {
        /// <summary>
        /// 判断集合是否为空
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this ICollection coll)
        {
            return coll == null || coll.Count == 0;
        }

        /// <summary>
        /// 判断集合是否有内容
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public static bool HasAny(this ICollection coll)
        {
            return coll != null && coll.Count > 0;
        }
    }
}
