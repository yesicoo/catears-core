﻿
namespace System.Collections.Generic
{
    /// <summary>
    /// 集合扩展方法
    /// </summary>
    public static class ExICollectionT
    {
        /// <summary>
        /// 判断集合是否为空
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public static bool IsNullOrEmptyT<T>(this ICollection<T> coll)
        {
            return coll == null || coll.Count == 0;
        }
        /// <summary>
        /// 判断集合是否有内容
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public static bool HasAnyT<T>(this ICollection<T> coll)
        {
            return coll != null && coll.Count > 0;
        }
    }
}
