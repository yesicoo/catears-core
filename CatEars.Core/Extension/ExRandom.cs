﻿
using CatEars.Core;

namespace System
{
    /// <summary>
    /// System.Random扩展
    /// </summary>
    public static class ExRandom
    {
        #region 随机生成字符串
        /// <summary>
        /// 八进制数字字符集合
        /// </summary>
        public static string NumOct = "01234567";
        /// <summary>
        /// 十进制数字字符集合
        /// </summary>
        public static string NumDec = "0123456789";
        /// <summary>
        /// 十六进制数字字符集合
        /// </summary>
        public static string NumHex = "0123456789ABCDEF";
        /// <summary>
        /// 小写字符集合
        /// </summary>
        public static string A2ZLower = "abcdefghijklmnopqrstuvwxyz";
        /// <summary>
        /// 大写字符集合
        /// </summary>
        public static string A2ZUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        /// <summary>
        /// 生成指定长度的随机字符串
        /// </summary>
        /// <param name="length">字符串长度</param>
        /// <param name="charset">备选字符</param>
        /// <returns></returns>
        public static string CreateRandomString(int length, string charset = null)
        {
            return AppEnv.Random.CreateRandomString(length, charset);
        }

        /// <summary>
        /// 生成指定长度的随机字符串
        /// </summary>
        /// <param name="random">随机数生成对象【NotNull】</param>
        /// <param name="length">字符串长度</param>
        /// <param name="charset">备选字符</param>
        /// <returns></returns>
        public static string CreateRandomString(this Random random, int length, string charset = null)
        {
            if (length <= 0)
            {
                return string.Empty;
            }
            if (charset == null)
            {
                charset = NumDec;
            }
            char[] chr = new char[length];
            for (int i = 0; i < length; i++)
            {
                chr[i] = charset[random.Next(charset.Length)];
            }
            return new string(chr);
        }
        #endregion
    }
}
