﻿
namespace System
{
    /// <summary>
    /// System.String 的扩展
    /// </summary>
    public static partial class ExString
    {
        #region 字符串对比
        /// <summary>
        /// 字符串对比 判断str1从intStartIndex开始是否与str2在intStr2StartIndex之后的字符完全一致
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="intStrStartIndex">str开始匹配的位置</param>
        /// <param name="strSub">子字符串</param>
        /// <param name="intStrSubStartIndex">strSub开始匹配的位置</param>
        /// <returns>返回值</returns>
        public static bool MatchSubString(
            this string str,
            int intStrStartIndex,
            string strSub,
            int intStrSubStartIndex)
        {
            if (str.Length - intStrStartIndex < strSub.Length - intStrSubStartIndex)
            {
                //长度不足
                return false;
            }
            bool blnSame = true;
            //这里从后往前比，后面的更容易匹配不上，效率微微高一点
            int intTmp = intStrStartIndex - intStrSubStartIndex;
            for (int i = strSub.Length - 1; i >= intStrSubStartIndex; i--)
            {
                if (str[intTmp + i] != strSub[i])
                {
                    blnSame = false;
                    break;
                }
            }
            return blnSame;
        }
        #endregion
    }
}
