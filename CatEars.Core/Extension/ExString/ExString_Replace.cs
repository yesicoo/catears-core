﻿
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace System
{
    /// <summary>
    /// System.String 的扩展
    /// </summary>
    public static partial class ExString
    {
        /// <summary>
        /// 将换行处理为空格
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ReplaceLineToSpace(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }
            bool isChange = false;
            StringBuilder strB = new StringBuilder(input[0]);
            for (int i = 0; i < input.Length; i++)
            {
                char chr = input[i];
                if (chr == '\r')
                {
                    isChange = true;
                    continue;
                }
                else if (chr == '\n')
                {
                    strB.Append(' ');
                    isChange = true;
                }
                else strB.Append(chr);
            }
            string strResult;
            if (isChange) strResult = strB.ToString();
            else strResult = input;
            return strResult;
        }
        
    }
}
