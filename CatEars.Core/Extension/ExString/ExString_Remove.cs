﻿
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace System
{
    /// <summary>
    /// System.String 的扩展
    /// </summary>
    public static partial class ExString
    {
        /// <summary>
        /// 删除字符串中的某个字符
        /// </summary>
        /// <param name="input">strInput</param>
        /// <param name="chr">chr</param>
        /// <returns>返回值</returns>
        public static string Remove(this string input, char chr)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }
            bool blnChange = false;
            StringBuilder strB = new StringBuilder(input[0]);
            for (int i = 1; i < input.Length; i++)
            {
                char chr1 = input[i];
                if (chr1 == chr)
                {
                    blnChange = true;
                    continue;
                }
                strB.Append(chr1);
            }
            if (blnChange) return strB.ToString();
            else return input;
        }

        /// <summary>
        /// 删除字符串中的某个字符
        /// </summary>
        /// <param name="input">strInput</param>
        /// <param name="chrs">chr</param>
        /// <returns>返回值</returns>
        public static string Remove(this string input, IEnumerable<char> chrs)
        {
            if (chrs == null) return input;
            return Remove(input, chrs.ToArray());
        }

        /// <summary>
        /// 删除字符串中的某个字符
        /// </summary>
        /// <param name="input">strInput</param>
        /// <param name="chrs">chr</param>
        /// <returns>返回值</returns>
        public static string Remove(this string input, params char[] chrs)
        {
            if (string.IsNullOrEmpty(input) || chrs.IsNullOrEmptyT()) return input;
            bool blnChange = false;
            StringBuilder strB = new StringBuilder(input[0]);
            HashSet<char> charss = new HashSet<char>(chrs);
            for (int i = 1; i < input.Length; i++)
            {
                char chr1 = input[i];
                if (charss.Contains(chr1))
                {
                    blnChange = true;
                    continue;
                }
                strB.Append(chr1);
            }
            if (blnChange) return strB.ToString();
            else return input;
        }

        #region 单字重复处理
        /// <summary>
        /// 删除重复的某个字符
        /// </summary>
        /// <param name="input">strInput</param>
        /// <param name="chr">chr</param>
        /// <returns>返回值</returns>
        public static string RemoveRepeat(this string input, char chr)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }
            bool blnChange = false;
            StringBuilder strB = new StringBuilder(input[0]);
            for (int i = 1; i < input.Length; i++)
            {
                char chr1 = input[i];
                char chr0 = input[i - 1];
                if (chr1 == chr0)
                {
                    if (chr1 == chr)
                    {
                        blnChange = true;
                        continue;
                    }
                }
                strB.Append(chr1);
            }
            if (blnChange) return strB.ToString();
            else return input;
        }

        /// <summary>
        /// 删除重复空格
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string RemoveRepeatSpace(this string input)
        {
            return RemoveRepeat(input, ' ');
        }
        #endregion
    }
}
