﻿

namespace System
{
    /// <summary>
    /// System.String 的扩展
    /// </summary>
    public static partial class ExString
    {
        /// <summary>
        /// !string.IsNullOrEmpty(str)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNotEmpty(this string str)
        {
            return !string.IsNullOrEmpty(str);
        }
        /// <summary>
        /// !string.IsNullOrWhiteSpace(str)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNotWhiteSpace(this string str)
        {
            return !string.IsNullOrWhiteSpace(str);
        }
    }
}
