﻿
namespace CatEars.Core.Numbers
{
    public partial class UnitConv
    {
        /// <summary>
        /// "毫秒", "秒", "分", "度"转换器
        /// </summary>
        public class ConvDegree : UnitConv
        {
            /// <summary>
            /// 单例对象
            /// </summary>
            public static ConvDegree Instance = new ConvDegree();

            /// <summary>
            /// 初始化
            /// </summary>
            public ConvDegree()
                : base(
                new double[] { 1000, 60, 60 },
                new string[] { "毫秒", "秒", "分", "度" })
            {
            }
        }

        /// <summary>
        /// "B", "KB", "MB", "GB", "TB" 转换器 进制为1024
        /// </summary>
        public class ConvByte : UnitConv
        {
            /// <summary>
            /// 单例对象
            /// </summary>
            public static ConvByte Instance = new ConvByte();

            /// <summary>
            /// 初始化
            /// </summary>
            public ConvByte()
                : base(
                new double[] { 1024, 1024, 1024, 1024 },
                new string[] { "B", "KB", "MB", "GB", "TB" })
            {
            }
        }

        /// <summary>
        /// "B", "KB", "MB", "GB", "TB" 转换器 进制为1000
        /// </summary>
        public class ConvByte0 : UnitConv
        {
            /// <summary>
            /// 单例对象
            /// </summary>
            public static ConvByte0 Instance = new ConvByte0();

            /// <summary>
            /// 初始化
            /// </summary>
            public ConvByte0()
                : base(
                new double[] { 1000, 1000, 1000, 1000 },
                new string[] { "B", "KB", "MB", "GB", "TB" })
            {
            }
        }

        /// <summary>
        /// "bit", "B", "KB", "MB", "GB", "TB" 转换器 进制为1024
        /// </summary>
        public class ConvByteEx : UnitConv
        {
            /// <summary>
            /// 单例对象
            /// </summary>
            public static ConvByteEx Instance = new ConvByteEx();

            /// <summary>
            /// 初始化
            /// </summary>
            public ConvByteEx()
                : base(
                new double[] { 8, 1024, 1024, 1024, 1024 },
                new string[] { "bit", "B", "KB", "MB", "GB", "TB" })
            {
            }
        }

        /// <summary>
        /// "bit", "B", "KB", "MB", "GB", "TB" 转换器 进制为1000
        /// </summary>
        public class ConvByte0Ex : UnitConv
        {
            /// <summary>
            /// 单例对象
            /// </summary>
            public static ConvByte0Ex Instance = new ConvByte0Ex();

            /// <summary>
            /// 初始化
            /// </summary>
            public ConvByte0Ex()
                : base(
                new double[] { 8, 1000, 1000, 1000, 1000 },
                new string[] { "bit", "B", "KB", "MB", "GB", "TB" })
            {
            }
        }
    }
}
