﻿
using System.Collections.Generic;
using System.Diagnostics;
using CatEars.Core.WinAPI;

namespace CatEars.Core.Net
{
    /// <summary>
    /// ADSL拨号相关
    /// </summary>
    public class ADSL
    {
        /// <summary>
        /// 获取所有宽带连接的名称
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllAdslName()
        {
            return RasInfo.GetAllRasEntryName();
        }

        /// <summary>
        /// 联网进程信息
        /// </summary>
        ProcessStartInfo _killProcessStartInfo;
        /// <summary>
        /// 联网进程信息
        /// </summary>
        ProcessStartInfo _connProcessStartInfo;
        /// <summary>
        /// 断网进程信息
        /// </summary>
        ProcessStartInfo _disconnProcessStartInfo;

        /// <summary>
        /// 初始化一个adsl拨号对象
        /// </summary>
        public ADSL()
        {
            _connProcessStartInfo = new ProcessStartInfo();
            _connProcessStartInfo.FileName = "rasdial.exe";
            _connProcessStartInfo.CreateNoWindow = true;
            _connProcessStartInfo.UseShellExecute = false;
            _connProcessStartInfo.RedirectStandardError = true;
            _connProcessStartInfo.RedirectStandardInput = true;
            _connProcessStartInfo.RedirectStandardOutput = true;

            _disconnProcessStartInfo = new ProcessStartInfo();
            _disconnProcessStartInfo.FileName = "rasdial.exe";
            _disconnProcessStartInfo.CreateNoWindow = true;
            _disconnProcessStartInfo.UseShellExecute = false;
            _disconnProcessStartInfo.RedirectStandardError = true;
            _disconnProcessStartInfo.RedirectStandardInput = true;
            _disconnProcessStartInfo.RedirectStandardOutput = true;

            _killProcessStartInfo = new ProcessStartInfo();
            _killProcessStartInfo.FileName = "taskkill";
            _killProcessStartInfo.Arguments = "/F /IM rasdial.exe";
            _killProcessStartInfo.CreateNoWindow = true;
            _killProcessStartInfo.UseShellExecute = false;
            _killProcessStartInfo.RedirectStandardError = true;
            _killProcessStartInfo.RedirectStandardInput = true;
            _killProcessStartInfo.RedirectStandardOutput = true;
        }

        /// <summary>
        /// 初始化一个adsl拨号对象
        /// </summary>
        /// <param name="strADSLName">拨号名称</param>
        /// <param name="strUserName">登录名</param>
        /// <param name="strPassword">登录密码</param>
        public ADSL(string strADSLName, string strUserName, string strPassword) : this()
        {
            ADSLName = strADSLName;
            UserName = strUserName;
            Password = strPassword;
        }

        /// <summary>
        /// 拨号名称
        /// </summary>
        public string ADSLName { get; set; }
        /// <summary>
        /// 登录名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 创建联网进程
        /// </summary>
        /// <returns>联网进程</returns>
        public Process CreateConnProcess()
        {
            _connProcessStartInfo.Arguments = this.ADSLName + " " + this.UserName + " " + this.Password;
            Process pro = new Process();
            pro.StartInfo = _connProcessStartInfo;
            return pro;
        }

        /// <summary>
        /// 创建断网进程
        /// </summary>
        /// <returns>断网进程</returns>
        public Process CreateDisconnProcess()
        {
            _disconnProcessStartInfo.Arguments = this.ADSLName + " /DISCONNECT";
            Process pro = new Process();
            pro.StartInfo = _disconnProcessStartInfo;
            return pro;
        }

        /// <summary>  
        /// 开始拨号  
        /// </summary>  
        public void Connect()
        {
            //拨号前杀掉前一个进程
            Kill();
            using (Process pro = CreateConnProcess())
            {
                pro.Start();
                pro.WaitForExit();
            }
        }

        /// <summary>  
        /// 断开连接  
        /// </summary>  
        public void Disconnect()
        {
            using (Process pro = CreateDisconnProcess())
            {
                pro.Start();
                pro.WaitForExit();
            }
        }

        /// <summary>  
        /// 断开连接  
        /// </summary>  
        public void Kill()
        {
            try
            {
                using (var p = Process.Start(_killProcessStartInfo))
                {
                    //等待15秒
                    p.WaitForExit(15000);
                }
            }
            //catch (Exception exc)
            catch
            {
                //忽略异常
            }
        }  
    }
}
