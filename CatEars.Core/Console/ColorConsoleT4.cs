﻿using System;

namespace CatEars.Core.Console
{
    public static partial class ColorConsole
    {
#pragma warning disable 1591
        public static void WriteDebug(string msg)
        {
            ConsoleReadWrite.Write(msg, WriteDebugColor);
        }
#pragma warning disable 1591
        public static void WriteDebug(string msg, params string[] args)
        {
            ConsoleReadWrite.Write(msg, args, WriteDebugColor);
        }
#pragma warning disable 1591
        public static void WriteDebugLine(string msg)
        {
            ConsoleReadWrite.WriteLine(msg, WriteDebugColor);
        }
#pragma warning disable 1591
        public static void WriteDebugLine(string msg, params string[] args)
        {
            ConsoleReadWrite.WriteLine(msg, args, WriteDebugColor);
        }

#pragma warning disable 1591
        public static void WriteInfo(string msg)
        {
            ConsoleReadWrite.Write(msg, WriteInfoColor);
        }
#pragma warning disable 1591
        public static void WriteInfo(string msg, params string[] args)
        {
            ConsoleReadWrite.Write(msg, args, WriteInfoColor);
        }
#pragma warning disable 1591
        public static void WriteInfoLine(string msg)
        {
            ConsoleReadWrite.WriteLine(msg, WriteInfoColor);
        }
#pragma warning disable 1591
        public static void WriteInfoLine(string msg, params string[] args)
        {
            ConsoleReadWrite.WriteLine(msg, args, WriteInfoColor);
        }

#pragma warning disable 1591
        public static void WriteWarn(string msg)
        {
            ConsoleReadWrite.Write(msg, WriteWarnColor);
        }
#pragma warning disable 1591
        public static void WriteWarn(string msg, params string[] args)
        {
            ConsoleReadWrite.Write(msg, args, WriteWarnColor);
        }
#pragma warning disable 1591
        public static void WriteWarnLine(string msg)
        {
            ConsoleReadWrite.WriteLine(msg, WriteWarnColor);
        }
#pragma warning disable 1591
        public static void WriteWarnLine(string msg, params string[] args)
        {
            ConsoleReadWrite.WriteLine(msg, args, WriteWarnColor);
        }

#pragma warning disable 1591
        public static void WriteError(string msg)
        {
            ConsoleReadWrite.Write(msg, WriteErrorColor);
        }
#pragma warning disable 1591
        public static void WriteError(string msg, params string[] args)
        {
            ConsoleReadWrite.Write(msg, args, WriteErrorColor);
        }
#pragma warning disable 1591
        public static void WriteErrorLine(string msg)
        {
            ConsoleReadWrite.WriteLine(msg, WriteErrorColor);
        }
#pragma warning disable 1591
        public static void WriteErrorLine(string msg, params string[] args)
        {
            ConsoleReadWrite.WriteLine(msg, args, WriteErrorColor);
        }

#pragma warning disable 1591
        public static void WriteFatal(string msg)
        {
            ConsoleReadWrite.Write(msg, WriteFatalColor);
        }
#pragma warning disable 1591
        public static void WriteFatal(string msg, params string[] args)
        {
            ConsoleReadWrite.Write(msg, args, WriteFatalColor);
        }
#pragma warning disable 1591
        public static void WriteFatalLine(string msg)
        {
            ConsoleReadWrite.WriteLine(msg, WriteFatalColor);
        }
#pragma warning disable 1591
        public static void WriteFatalLine(string msg, params string[] args)
        {
            ConsoleReadWrite.WriteLine(msg, args, WriteFatalColor);
        }

#pragma warning disable 1591
        public static void WriteSuccess(string msg)
        {
            ConsoleReadWrite.Write(msg, WriteSuccessColor);
        }
#pragma warning disable 1591
        public static void WriteSuccess(string msg, params string[] args)
        {
            ConsoleReadWrite.Write(msg, args, WriteSuccessColor);
        }
#pragma warning disable 1591
        public static void WriteSuccessLine(string msg)
        {
            ConsoleReadWrite.WriteLine(msg, WriteSuccessColor);
        }
#pragma warning disable 1591
        public static void WriteSuccessLine(string msg, params string[] args)
        {
            ConsoleReadWrite.WriteLine(msg, args, WriteSuccessColor);
        }

    }
    public partial class ColorConsoleEx
    {
#pragma warning disable 1591
        public void WriteDebug(string msg)
        {
            ColorConsole.WriteDebug(GetPrefix("Debug") + msg);
        }
#pragma warning disable 1591
        public void WriteDebug(string msg, params string[] args)
        {
            ColorConsole.WriteDebug(GetPrefix("Debug") + msg, args);
        }
#pragma warning disable 1591
        public void WriteDebugLine(string msg)
        {
            ColorConsole.WriteDebugLine(GetPrefix("Debug") + msg);
        }
#pragma warning disable 1591
        public void WriteDebugLine(string msg, params string[] args)
        {
            ColorConsole.WriteDebugLine(GetPrefix("Debug") + msg, args);
        }

#pragma warning disable 1591
        public void WriteInfo(string msg)
        {
            ColorConsole.WriteInfo(GetPrefix("Info") + msg);
        }
#pragma warning disable 1591
        public void WriteInfo(string msg, params string[] args)
        {
            ColorConsole.WriteInfo(GetPrefix("Info") + msg, args);
        }
#pragma warning disable 1591
        public void WriteInfoLine(string msg)
        {
            ColorConsole.WriteInfoLine(GetPrefix("Info") + msg);
        }
#pragma warning disable 1591
        public void WriteInfoLine(string msg, params string[] args)
        {
            ColorConsole.WriteInfoLine(GetPrefix("Info") + msg, args);
        }

#pragma warning disable 1591
        public void WriteWarn(string msg)
        {
            ColorConsole.WriteWarn(GetPrefix("Warn") + msg);
        }
#pragma warning disable 1591
        public void WriteWarn(string msg, params string[] args)
        {
            ColorConsole.WriteWarn(GetPrefix("Warn") + msg, args);
        }
#pragma warning disable 1591
        public void WriteWarnLine(string msg)
        {
            ColorConsole.WriteWarnLine(GetPrefix("Warn") + msg);
        }
#pragma warning disable 1591
        public void WriteWarnLine(string msg, params string[] args)
        {
            ColorConsole.WriteWarnLine(GetPrefix("Warn") + msg, args);
        }

#pragma warning disable 1591
        public void WriteError(string msg)
        {
            ColorConsole.WriteError(GetPrefix("Error") + msg);
        }
#pragma warning disable 1591
        public void WriteError(string msg, params string[] args)
        {
            ColorConsole.WriteError(GetPrefix("Error") + msg, args);
        }
#pragma warning disable 1591
        public void WriteErrorLine(string msg)
        {
            ColorConsole.WriteErrorLine(GetPrefix("Error") + msg);
        }
#pragma warning disable 1591
        public void WriteErrorLine(string msg, params string[] args)
        {
            ColorConsole.WriteErrorLine(GetPrefix("Error") + msg, args);
        }

#pragma warning disable 1591
        public void WriteFatal(string msg)
        {
            ColorConsole.WriteFatal(GetPrefix("Fatal") + msg);
        }
#pragma warning disable 1591
        public void WriteFatal(string msg, params string[] args)
        {
            ColorConsole.WriteFatal(GetPrefix("Fatal") + msg, args);
        }
#pragma warning disable 1591
        public void WriteFatalLine(string msg)
        {
            ColorConsole.WriteFatalLine(GetPrefix("Fatal") + msg);
        }
#pragma warning disable 1591
        public void WriteFatalLine(string msg, params string[] args)
        {
            ColorConsole.WriteFatalLine(GetPrefix("Fatal") + msg, args);
        }

#pragma warning disable 1591
        public void WriteSuccess(string msg)
        {
            ColorConsole.WriteSuccess(GetPrefix("Success") + msg);
        }
#pragma warning disable 1591
        public void WriteSuccess(string msg, params string[] args)
        {
            ColorConsole.WriteSuccess(GetPrefix("Success") + msg, args);
        }
#pragma warning disable 1591
        public void WriteSuccessLine(string msg)
        {
            ColorConsole.WriteSuccessLine(GetPrefix("Success") + msg);
        }
#pragma warning disable 1591
        public void WriteSuccessLine(string msg, params string[] args)
        {
            ColorConsole.WriteSuccessLine(GetPrefix("Success") + msg, args);
        }

    }
}
