﻿using System;

namespace CatEars.Core.Console
{
    /// <summary>
    /// 在CColorConsole的基础上添加前缀
    /// </summary>
    public partial class ColorConsoleEx
    {
        #region Static
        /// <summary>
        /// 输出“时间 + 正常内容”的CColorConsole
        /// </summary>
        public static ColorConsoleEx WithTime = new ColorConsoleEx()
        {
            GetPrefixFunc = (string t) =>
            {
                return DateTime.Now.ToDateTimeFormat() + " ";
            },
        };
        /// <summary>
        /// 输出“类型 + 正常内容”的CColorConsole
        /// </summary>
        public static ColorConsoleEx WithTypeName = new ColorConsoleEx()
        {
            GetPrefixFunc = (string t) =>
            {
                return t + " : ";
            },
        };
        /// <summary>
        /// 输出“时间 + 类型 + 正常内容”的CColorConsole
        /// </summary>
        public static ColorConsoleEx WithTimeAndTypeName = new ColorConsoleEx()
        {
            Parent = WithTime,
            GetPrefixFunc = (string t) =>
            {
                return t + " : ";
            },
        };

        #endregion

        #region Static AskInput

        /// <summary>
        /// 按回车键结束
        /// </summary>
        public static void EnterToContinue()
        {
            ColorConsole.AskInput("按Enter键继续");
        }

        /// <summary>
        /// 按任意键结束
        /// </summary>
        public static void AnyKeyToContinue()
        {
            ColorConsole.ConsoleReadWrite.Write("按任意键结束",
                ColorConsole.InputAskColor, ColorConsole.InputAskBackColor);
            ColorConsole.ConsoleReadWrite.ReadKey();
        }
        /// <summary>
        /// 按回车键结束
        /// </summary>
        public static void EnterToExit()
        {
            ColorConsole.AskInput("按Enter键结束");
            ColorConsole.WriteInfoLine("程序即将退出");
        }

        /// <summary>
        /// 按任意键结束
        /// </summary>
        public static void AnyKeyToExit()
        {
            ColorConsole.ConsoleReadWrite.Write("按任意键结束",
                ColorConsole.InputAskColor, ColorConsole.InputAskBackColor);
            ColorConsole.ConsoleReadWrite.ReadKey();
            ColorConsole.WriteInfoLine(ColorConsole.NewLine + "程序即将退出");
        }

        #endregion

        /// <summary>
        /// 当前CColorConsoleEx 的父级对象（组合输出前缀）
        /// </summary>
        public ColorConsoleEx Parent { get; set; }

        /// <summary>
        /// Prefix 和 GetPrefixAction 二选一，优先使用 GetPrefixAction
        /// </summary>
        public string Prefix { get; set; }
        /// <summary>
        /// Prefix 和 GetPrefixAction 二选一，优先使用 GetPrefixAction
        /// </summary>
        public Func<string, string> GetPrefixFunc { get; set; }

        /// <summary>
        /// 获取要显示在消息之前的前缀
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        string GetPrefix(string type)
        {
            string parent = Parent?.GetPrefix(type);
            if (GetPrefixFunc != null) return parent + GetPrefixFunc.Invoke(type);
            else return parent + Prefix;
        }
    }
}
