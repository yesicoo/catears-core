﻿
using CatEars.Core.Logs;

namespace CatEars.Core.Console
{
    /// <summary>
    /// ColorConsole日志
    /// </summary>
#pragma warning disable 1591
    public class ColorConsoleLog : LogBase, ILog
    {
        readonly ColorConsoleEx _console;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="console">【NotNull】</param>
        public ColorConsoleLog(ColorConsoleEx console) : this(console, null) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="console">【NotNull】</param>
        /// <param name="parent"></param>
        public ColorConsoleLog(ColorConsoleEx console, ILog parent) : base(parent) { _console = console; }

        public override void Debug(string msg)
        {
            base.Debug(msg);
            _console.WriteDebugLine(msg);
        }
        
        public override void Info(string msg)
        {
            base.Info(msg);
            _console.WriteInfoLine(msg);
        }

        public override void Warn(string msg)
        {
            base.Warn(msg);
            _console.WriteWarnLine(msg);
        }
        
        public override void Error(string msg)
        {
            base.Error(msg);
            _console.WriteErrorLine(msg);
        }
        
        public override void Fatal(string msg)
        {
            base.Fatal(msg);
            _console.WriteFatalLine(msg);
        }
        
        public override void Success(string msg)
        {
            base.Success(msg);
            _console.WriteSuccessLine(msg);
        }
    }
}
