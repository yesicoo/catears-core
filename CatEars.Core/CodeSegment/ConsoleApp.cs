﻿#if DEBUG
using System;
using CatEars.Core;
using CatEars.Core.Console;

namespace CodeSegment
{
    /// <summary>
    /// Console工程
    /// </summary>
    static class ConsoleApp1
    {
        /// <summary>
        /// 应用程序入口点
        /// </summary>
        /// <param name="args">运行参数</param>
        static void Main(string[] args)
        {
            try
            {
                AppEnv.InitConsole();
                ColorConsoleEx.AnyKeyToExit();
            }
            catch (Exception exc)
            {
                AppEnv.Log.Fatal(exc);
                ColorConsoleEx.EnterToExit();
            }
        }
    }

    /// <summary>
    /// Console工程
    /// </summary>
    static class ConsoleApp2
    {
        /// <summary>
        /// 应用程序入口点
        /// </summary>
        /// <param name="args">运行参数</param>
        static void Main(string[] args)
        {
            try
            {
                AppEnv.InitConsole();
                Console.WriteLine("按任意键结束");
                Console.ReadKey();
                Console.WriteLine("\r\n程序即将退出");
            }
            catch (Exception exc)
            {
                Console.WriteLine("{0} 出错了: {1}", DateTime.Now, exc.Message);
                Console.WriteLine(exc.StackTrace);
                Console.WriteLine("按Enter键结束");
                Console.ReadLine();
                Console.WriteLine("程序即将退出");
            }
        }
    }
}
#endif
