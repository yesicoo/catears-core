﻿#if DEBUG
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using CatEars.Core;
using CatEars.Core.Utility;

namespace CodeSegment
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    partial class WPFApp1 : Application
    {
        /// <summary>
        /// 
        /// </summary>
        public WPFApp1()
        {
            this.DispatcherUnhandledException += App_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            AppEnv.Init();
            //限制单实例
            SingletonProcess.CheckByMutex(null, true, 3);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exc = e.ExceptionObject as Exception;
            if (exc == null) return;
            MessageBox.Show(exc.Message, "错误", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "错误", MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
        }
    }
    /// <summary>
    /// WPF工程
    /// </summary>
    static class WPFApp2
    {
        /// <summary>
        /// App.xaml 的交互逻辑
        /// </summary>
        partial class App : Application
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="args"></param>
            [STAThread]
            public static void Main(string[] args)
            {
                AppEnv.Init();
                //限制单实例
                SingletonProcess.CheckByMutex(null, true, 3);

                App app = new App();
                app.Run();
            }

            public App()
            {
                this.Startup += App_Startup;
                this.DispatcherUnhandledException += App_DispatcherUnhandledException;
            }

            private void App_Startup(object sender, StartupEventArgs e)
            {
                var window = new Window();
                this.MainWindow = window;
                this.MainWindow.Show();
            }

            void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
            {
                MessageBox.Show(e.Exception.Message);
                e.Handled = true;
            }
        }
    }
}
#endif