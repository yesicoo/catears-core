﻿#if DEBUG
using System;
using System.Windows.Forms;
using CatEars.Core;
using CatEars.Core.Utility;

namespace CodeSegment
{
    /// <summary>
    /// WinForm工程
    /// </summary>
    static class WinFormApp
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                //全局异常捕捉
                Application.ThreadException += Application_ThreadException; //UI线程异常
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException; //多线程异常

                AppEnv.Init();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                //限制单实例
                SingletonProcess.CheckByMutex(null, true, 2);

                //打开主界面
                Application.Run(new Form());
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString(), "错误信息");
            }
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.ToString(), "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(((Exception)e.ExceptionObject).ToString(), "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
#endif
