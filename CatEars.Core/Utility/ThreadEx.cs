﻿
using System.Threading;

namespace CatEars.Core.Utility
{
    /// <summary>
    /// Thread扩展
    /// </summary>
    public static class ThreadEx
    {
        /// <summary>
        /// 将当前线程挂起指定的时间
        /// </summary>
        /// <param name="millisecondsTimeout">线程被阻塞的毫秒数。指定零 (0) 以指示应挂起此线程以使其他等待线程能够执行。指定 System.Threading.Timeout.Infinite。以无限期阻止线程。</param>
        public static void SleepShort(int millisecondsTimeout)
        {
#if DEBUG
            //防止调试时进入一个很长时间的休眠导致调试不方便，将休眠时间以2000为单位进行休眠
            if (millisecondsTimeout > 5000)
            {
                int intTotal = millisecondsTimeout;
                int intStep = 2000;
                while (intTotal > intStep)
                {
                    Thread.Sleep(intStep);
                    intTotal -= intStep;
                }
                Thread.Sleep(intTotal);
                return;
            }
#endif
            Thread.Sleep(millisecondsTimeout);
        }
    }
}
