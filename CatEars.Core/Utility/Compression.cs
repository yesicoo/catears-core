﻿
using System.IO;
using System.IO.Compression;

namespace CatEars.Core.Utility
{
    /// <summary>
    /// 流压缩
    /// </summary>
    public static class Compression
    {
        /// <summary>
        /// GZip压缩
        /// </summary>
        public static GZipCompression GZip = new GZipCompression();
        /// <summary>
        /// Deflate压缩
        /// </summary>
        public static DeflateCompression Deflate = new DeflateCompression();

        /// <summary>
        /// GZip压缩
        /// </summary>
#pragma warning disable 1591
        public class GZipCompression
        {
            public void Compress(Stream from, Stream to)
            {
                using (var cmp = new GZipStream(to, CompressionMode.Compress))
                {
                    from.CopyToStream(cmp);
                    cmp.Close();
                }
            }

            public void Compress(byte[] data, Stream to)
            {
                using (var cmp = new GZipStream(to, CompressionMode.Compress))
                {
                    cmp.Write(data, 0, data.Length);
                    cmp.Close();
                }
            }
 
            public byte[] Compress(byte[] data)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    Compress(data, ms);
                    return ms.ToArray();
                }
            }

            public void Decompress(Stream from, Stream to)
            {
                using (var cmp = new GZipStream(from, CompressionMode.Decompress))
                {
                    cmp.CopyToStream(to);
                    cmp.Close();
                }
            }

            public void Decompress(byte[] data, Stream to)
            {
                using (MemoryStream ms = new MemoryStream(data))
                {
                    Decompress(ms, to);
                }
            }

            public byte[] Decompress(byte[] data)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    Compress(data, ms);
                    return ms.ToArray();
                }
            }
        }

        /// <summary>
        /// Deflate压缩
        /// </summary>
#pragma warning disable 1591
        public class DeflateCompression
        {
            //从GZip拷贝过来的
            public void Compress(Stream from, Stream to)
            {
                using (var cmp = new DeflateStream(to, CompressionMode.Compress))
                {
                    from.CopyToStream(cmp);
                    cmp.Close();
                }
            }

            public void Compress(byte[] data, Stream to)
            {
                using (var cmp = new DeflateStream(to, CompressionMode.Compress))
                {
                    cmp.Write(data, 0, data.Length);
                    cmp.Close();
                }
            }

            public byte[] Compress(byte[] data)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    Compress(data, ms);
                    return ms.ToArray();
                }
            }

            public void Decompress(Stream from, Stream to)
            {
                using (var cmp = new DeflateStream(from, CompressionMode.Decompress))
                {
                    cmp.CopyToStream(to);
                    cmp.Close();
                }
            }

            public void Decompress(byte[] data, Stream to)
            {
                using (MemoryStream ms = new MemoryStream(data))
                {
                    Decompress(ms, to);
                }
            }

            public byte[] Decompress(byte[] data)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    Compress(data, ms);
                    return ms.ToArray();
                }
            }
        }
    }
}
