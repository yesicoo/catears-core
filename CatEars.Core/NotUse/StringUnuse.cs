﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CatEars.Text
{
    #region 截取字符串
    ///// <summary>
    ///// 截取strStart和strEnd间的字符串
    ///// </summary>
    ///// <param name="strSource">要处理的字符串</param>
    ///// <param name="chrStart">开始字符</param>
    ///// <param name="chrEnd">结束字符</param>
    ///// <param name="blnContainChar">返回值是否包含开始字符和结束字符</param>
    ///// <returns>返回值</returns>
    //public static string GetStringBetween(
    //    string strSource,
    //    char chrStart,
    //    char chrEnd,
    //    bool blnContainChar)
    //{
    //    return GetStringBetween(strSource, chrStart, chrEnd, blnContainChar, strSource);
    //}

    ///// <summary>
    ///// 截取strStart和strEnd间的字符串
    ///// </summary>
    ///// <param name="strSource">要处理的字符串</param>
    ///// <param name="chrStart">开始字符</param>
    ///// <param name="chrEnd">结束字符</param>
    ///// <param name="blnContainChar">返回值是否包含开始字符和结束字符</param>
    ///// <param name="strDefault">找不到指定字符串返回的值</param>
    ///// <returns>返回值</returns>
    //public static string GetStringBetween(
    //    string strSource,
    //    char chrStart,
    //    char chrEnd,
    //    bool blnContainChar,
    //    string strDefault)
    //{
    //    if (string.IsNullOrEmpty(strSource))
    //    {
    //        return strDefault;
    //    }
    //    int intIndex1 = strSource.IndexOf(chrStart);
    //    if (intIndex1 < 0 || intIndex1 == strSource.Length - 1)
    //    {
    //        return strDefault;
    //    }
    //    int intIndex2 = strSource.LastIndexOf(chrEnd);
    //    if (intIndex2 <= intIndex1)
    //    {
    //        return strDefault;
    //    }
    //    if (blnContainChar)
    //    {
    //        return strSource.Substring(intIndex1, intIndex2 - intIndex1 + 1);
    //    }
    //    else
    //    {
    //        return strSource.Substring(intIndex1 + 1, intIndex2 - intIndex1 - 1);
    //    }
    //}
    #endregion

    #region 移除重复子串
    ///// <summary>
    ///// 移除重复子串（不连续移除）
    ///// </summary>
    ///// <param name="strInput">输入字符串</param>
    ///// <param name="intMinRepetLength">至少多少字连接算重复</param>
    ///// <returns>返回值</returns>
    //public static string RemoveRepetSubString(string strInput, int intMinRepetLength)
    //{
    //    try
    //    {
    //        if (intMinRepetLength <= 0)
    //        {
    //            return strInput;
    //        }
    //        string strInput0 = strInput;
    //    Start:
    //        int intL = strInput0.Length;
    //        for (int intLoc1 = 0; intLoc1 + intMinRepetLength + intMinRepetLength <= intL; intLoc1++)
    //        {
    //            int intLength = intMinRepetLength;
    //            for (; intLoc1 + intLength + intMinRepetLength <= intL; intLength++)
    //            {
    //                int intLoc2 = intLoc1 + intLength;
    //                int intSameCount = 0;
    //                for (int intIndex = 0; intLoc2 + intIndex < intL; intIndex++)
    //                {
    //                    if (strInput0[intLoc1 + intIndex] == strInput0[intLoc2 + intIndex])
    //                    {
    //                        intSameCount++;
    //                    }
    //                    else
    //                    {
    //                        break;
    //                    }
    //                }
    //                if (intSameCount >= intMinRepetLength)
    //                {
    //                    strInput0 = strInput0.Substring(0, intLoc2)
    //                        + strInput0.Substring(intLoc2 + intSameCount);
    //                    goto Start;
    //                }
    //            }
    //        }
    //        return strInput0;
    //    }
    //    catch (Exception exc)
    //    {
    //        //多处按索引取字符，容易出错，只记日志，不抛异常
    //        Console.WriteLine(System.DateTime.Now.ToString() + "\t" + exc.ToString());
    //        return strInput;
    //    }
    //}

    ///// <summary>
    ///// 移除重复子串（连续移除）
    ///// </summary>
    ///// <param name="strInput">输入字符串</param>
    ///// <param name="intMinRepetLength">至少多少字连接算重复</param>
    ///// <returns>返回值</returns>
    //public static string RemoveNearRepetSubString(string strInput, int intMinRepetLength)
    //{
    //    try
    //    {
    //        if (intMinRepetLength <= 0)
    //        {
    //            return strInput;
    //        }
    //        string strInput0 = strInput;
    //    Start:
    //        int intL = strInput0.Length;
    //        for (int intLoc1 = 0; intLoc1 + intMinRepetLength + intMinRepetLength <= intL; intLoc1++)
    //        {
    //            for (int intLoc2 = intLoc1 + intMinRepetLength; intLoc2 + intLoc2 - intLoc1 <= intL; intLoc2++)
    //            {
    //                int intLen = intLoc2 - intLoc1;
    //                bool blnSame = true;
    //                for (int i = 0; i < intLen; i++)
    //                {
    //                    if (strInput0[intLoc1 + i] != strInput0[intLoc2 + i])
    //                    {
    //                        blnSame = false;
    //                        break;
    //                    }
    //                }
    //                if (blnSame)
    //                {
    //                    string strRemoved = strInput0.Substring(intLoc1 + intLen, intLen);
    //                    double dblTmp = 0;
    //                    //纯数字不移除
    //                    if (!double.TryParse(strRemoved, out dblTmp))
    //                    {
    //                        strInput0 = strInput0.Substring(0, intLoc1 + intLen)
    //                            + strInput0.Substring(intLoc2 + intLen);
    //                        goto Start;
    //                    }
    //                }
    //            }
    //        }
    //        return strInput0;
    //    }
    //    catch (Exception exc)
    //    {
    //        //多处按索引取字符，容易出错，只记日志，不抛异常
    //        Console.WriteLine(System.DateTime.Now.ToString() + "\t" + exc.ToString());
    //        return strInput;
    //    }
    //}
    #endregion
}
