﻿
using System;
using System.IO;
using CatEars.Core.FileFormat.Csv;

namespace CatEars.Core.Data.KvParam
{
#pragma warning disable 1591
    public class CsvSettingProvider : SettingProvider
    {
        /// <summary>
        /// Csv格式定义
        /// </summary>
        public CsvDefine CsvDefine { get; set; }

        /// <summary>
        /// 初始化
        /// </summary>
        public CsvSettingProvider()
        {
            CsvDefine = CsvDefine.Default.Clone();
            CsvDefine.WriteBorderForAllCell = true;
            FileExt = ".csv";
        }

        /// <summary>
        /// 默认文件夹
        /// </summary>
        /// <returns></returns>
        protected override string GetDefaultFolderName()
        {
            return Path.Combine(AppEnv.ExeDirPath, "Config");
        }

        /// <summary>
        /// 加载参数
        /// </summary>
        /// <param name="strFilePath">存储路径</param>
        /// <returns>加载的参数</returns>
        protected override ZSettingParams LoadFromPath(string strFilePath)
        {
            ZSettingParams p = base.CreateParam();
            try
            {
                string strFilePath0 = strFilePath;
                using (var sr = CsvReader.OpenFile(CsvDefine, strFilePath0))
                {
                    //读取表头
                    CsvCore.ReadOneCsvRow(CsvDefine, sr);
                    var rows = CsvCore.ReadCsvToEnd(CsvDefine, sr);
                    foreach (var item in rows)
                    {
                        if (item.Length == 3)
                        {
                            p.Gw(item[0]).Set(item[1],item[2]);
                        }
                        else if (!SafeMode)
                        {
                            throw new NotSupportedException(
                                string.Format("文件“{0}”格式不正确", strFilePath0));
                        }
                    }
                }
            }
            catch
            {
                if (!SafeMode) throw;
            }
            return p;
        }
        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="strFilePath">存储路径</param>
        /// <param name="p">要保存的参数</param>
        protected override void SaveToPath(string strFilePath, ZSettingParams p)
        {
            string strFilePath0 = strFilePath;
            try
            {
                if (p == null || p.Count == 0)
                {
                    if (File.Exists(strFilePath0))
                    {
                        File.Delete(strFilePath0);
                    }
                    return;
                }
                using (var sw = CsvWriter.OpenFile(CsvDefine, strFilePath0))
                {
                    string[] strBuffer = new string[3] { "Group", "Key", "Value" };
                    //写入表头
                    CsvCore.WriteOneCsvRow(CsvDefine, sw, strBuffer, false);
                    foreach (string strPName in p.Params)
                    {
                        var pKv = p.Gr(strPName);
                        var pKeys = pKv.Keys;
                        foreach (string strKey in pKeys)
                        {
                            strBuffer[0] = strPName;
                            strBuffer[1] = strKey;
                            strBuffer[2] = pKv[strKey];
                            CsvCore.WriteOneCsvRow(CsvDefine, sw, strBuffer, false);
                        }
                    }
                    sw.Flush();
                }
            }
            catch
            {
                if (!SafeMode)
                {
                    throw;
                }
            }
        }
    }
}
