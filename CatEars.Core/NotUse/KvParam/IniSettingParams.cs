﻿
using System.Collections.Generic;
using System.Diagnostics;
using CatEars.Core.FileFormat.Ini;

namespace CatEars.Core.Data.KvParam
{
    /// <summary>
    /// 参数组
    /// </summary>
    [DebuggerDisplay("Count={Count}")]
    class IniSettingParams : ZSettingParams
    {
        internal IniFile IniFile { get; private set; }

        public IniSettingParams()
        {
            IniFile = new IniFile();
        }

        public IniSettingParams(IniFile file)
        {
            IniFile = file;
        }

        /// <summary>
        /// 同步对象
        /// </summary>
        protected override object Sync { get { return IniFile.Sync; } }

        /// <summary>
        /// 数量
        /// </summary>
        public override int Count { get { return IniFile.SectionCount; } }

        /// <summary>
        /// 获取所有ZSettingParam的名称
        /// </summary>
        public override ICollection<string> Params { get { return IniFile.SectionNames; } }

        /// <summary>
        /// 获取一个参数对象
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        protected override ParamBase XGetParam(string strParamName)
        {
            return IniFile[strParamName];
        }

        /// <summary>
        /// 添加一个参数对象
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        protected override ParamBase XAddParam(string strParamName)
        {
            return IniFile.AddSection(strParamName);
        }

        /// <summary>
        /// 删除一个参数对象
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        protected override bool XRemoveParam(string strParamName)
        {
            return IniFile.RemoveSection(strParamName);
        }

        //IniFile.Gr会创建新对象 不重写它
        ///// <summary>
        ///// 获取一个Group用于读参数
        ///// </summary>
        ///// <param name="strParamName">strParamName</param>
        ///// <returns>ZSettingParam</returns>
        //public override ZSettingParam Gr(string strParamName)
        //{
        //    return IniFile.Gr(strParamName);
        //}

        /// <summary>
        /// 获取一个Group用于写参数
        /// </summary>
        /// <param name="strParamName">strParamName</param>
        /// <returns>ZSettingParam</returns>
        public override ParamBase Gw(string strParamName)
        {
            return IniFile.Gw(strParamName);
        }

        /// <summary>
        /// 添加一个Param
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        public override ParamBase AddParam(string strParamName)
        {
            return IniFile.AddSection(strParamName);
        }

        /// <summary>
        /// 删除一个Group
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        public override bool RemoveParam(string strParamName)
        {
            return IniFile.RemoveSection(strParamName);
        }
    }
}
