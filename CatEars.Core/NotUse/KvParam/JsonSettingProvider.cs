﻿
using System.IO;
using System.Text;

namespace CatEars.Core.Data.KvParam
{
#pragma warning disable 1591
    public class JsonSettingProvider : ZSettingProvider
    {
        /// <summary>
        /// 读写文本编码
        /// </summary>
        public string EncodingName { get; set; }
        /// <summary>
        /// 初始化
        /// </summary>
        public JsonSettingProvider()
        {
            FileExt = ".json";
            EncodingName = "UTF-8";
        }

        /// <summary>
        /// 默认文件夹
        /// </summary>
        /// <returns></returns>
        protected override string GetDefaultFolderName()
        {
            return Path.Combine(AppEnv.ExeDirPath, "Config");
        }

        /// <summary>
        /// 加载参数
        /// </summary>
        /// <param name="strKey">标识符</param>
        /// <returns>加载的参数</returns>
        public T Load<T>(string strKey)
        {
            T value;
            Load(strKey, out value);
            return value;
        }

        /// <summary>
        /// 加载参数
        /// </summary>
        /// <param name="strKey">标识符</param>
        /// <param name="value">输出对象</param>
        /// <returns>是否成功加载</returns>
        public bool Load<T>(string strKey, out T value)
        {
            string strFileName = GetTargetFileFullName(strKey, false);
            try
            {
                string strJson = null;
                if (File.Exists(strFileName))
                {
                    if (string.IsNullOrEmpty(EncodingName)) strJson = File.ReadAllText(strFileName);
                    else strJson = File.ReadAllText(strFileName, Encoding.GetEncoding(EncodingName));
                }
                if (!string.IsNullOrEmpty(strJson))
                {
                    value = default(T);
                    //value = Json.ToObject<T>(strJson);
                    return true;
                }
            }
            catch
            {
                if (!SafeMode) throw;
            }
            value = default(T);
            return false;
        }

        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="strKey">标识符</param>
        /// <param name="p">要保存的参数</param>
        public void Save(string strKey, object obj)
        {
            string strFileName = GetTargetFileFullName(strKey, true);
            try
            {
                if (obj == null)
                {
                    if (File.Exists(strFileName))
                    {
                        File.Delete(strFileName);
                    }
                    return;
                }
                string strJson = null;
                //string strJson = Json.ToNiceJSON(obj);
                if (string.IsNullOrEmpty(EncodingName)) File.WriteAllText(strFileName, strJson);
                else File.WriteAllText(strFileName, strJson, Encoding.GetEncoding(EncodingName));
            }
            catch
            {
                if (!SafeMode) throw;
            }
        }

        /// <summary>
        /// 加载参数
        /// </summary>
        /// <param name="objKey">标识符</param>
        /// <returns>加载的参数</returns>
        public T Load<T>(object objKey)
        {
            T value;
            Load(objKey, out value);
            return value;
        }

        /// <summary>
        /// 加载参数
        /// </summary>
        /// <param name="objKey">标识符</param>
        /// <param name="value">输出对象</param>
        /// <returns>是否成功加载</returns>
        public bool Load<T>(object objKey, out T value)
        {
            string strKey = GetKey(objKey);
            string strFileName = GetTargetFileFullName(strKey, false);
            try
            {
                string strJson = null;
                if (File.Exists(strFileName))
                {
                    if (string.IsNullOrEmpty(EncodingName)) strJson = File.ReadAllText(strFileName);
                    else strJson = File.ReadAllText(strFileName, Encoding.GetEncoding(EncodingName));
                }
                if (!string.IsNullOrEmpty(strJson))
                {
                    value = default(T);
                    //value = Json.ToObject<T>(strJson);
                    return true;
                }
            }
            catch
            {
                if (!SafeMode) throw;
            }
            value = default(T);
            return false;
        }

        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="objKey">标识符</param>
        /// <param name="p">要保存的参数</param>
        public void Save(object objKey, object obj)
        {
            string strKey = GetKey(objKey);
            string strFileName = GetTargetFileFullName(strKey, true);
            try
            {
                if (obj == null)
                {
                    if (File.Exists(strFileName))
                    {
                        File.Delete(strFileName);
                    }
                    return;
                }
                string strJson = null;
                //string strJson = Json.ToNiceJSON(obj);
                if (string.IsNullOrEmpty(EncodingName)) File.WriteAllText(strFileName, strJson);
                else File.WriteAllText(strFileName, strJson, Encoding.GetEncoding(EncodingName));
            }
            catch
            {
                if (!SafeMode) throw;
            }
        }
    }
}
