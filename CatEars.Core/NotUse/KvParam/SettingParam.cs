﻿
using System.Collections.Generic;
using System.Diagnostics;

namespace CatEars.Core.Data.KvParam
{
    /// <summary>
    /// 参数对象
    /// </summary>
    [DebuggerDisplay("Count={Count}")]
    public class SettingParam : ParamBase
    {
        /// <summary>
        /// 参数对象
        /// </summary>
        public SettingParam()
        {
            InnerDict = new Dictionary<string, string>();
        }

        /// <summary>
        /// 内部字典
        /// </summary>
        public Dictionary<string, string> InnerDict { get; private set; }

        /// <summary>
        /// 数量
        /// </summary>
        public override int Count { get { return InnerDict.Count; } }

        /// <summary>
        /// 获取已有的Key
        /// </summary>
        public override ICollection<string> Keys
        {
            get { return InnerDict.Keys; }
        }

        #region 抽象方法
        /// <summary>
        /// 清空
        /// </summary>
        public override void Clear()
        {
            InnerDict.Clear();
        }
        /// <summary>
        /// 取值
        /// </summary>
        /// <param name="strKey">key</param>
        /// <param name="strDefault">默认值</param>
        /// <returns>value</returns>
        protected override string XGet(string strKey, string strDefault = null)
        {
            return InnerDict.GetOrDefault(strKey, strDefault);
        }

        /// <summary>
        /// 设置值
        /// </summary>
        /// <param name="strKey">key</param>
        /// <param name="strValue">value</param>
        protected override void XSet(string strKey, string strValue)
        {
            //不自动删除null项，处理为空字符串
            //if (strValue == null) InnerDict.Remove(strKey);
            //else InnerDict[strKey] = strValue.ToString();
            string strValue2 = strValue ?? "";
            InnerDict[strKey] = strValue2;
        }
        #endregion
    }
}
