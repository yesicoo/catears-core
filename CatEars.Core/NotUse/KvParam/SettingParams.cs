﻿
using System.Collections.Generic;
using System.Diagnostics;
using CatEars.Core.Collections;

namespace CatEars.Core.Data.KvParam
{
    /// <summary>
    /// 参数组
    /// </summary>
    [DebuggerDisplay("Count={Count}")]
    public class SettingParams : ZSettingParams
    {
        /// <summary>
        /// 存储结构
        /// </summary>
        DictEx<string, SettingParam> _params = new DictEx<string, SettingParam>();

        /// <summary>
        /// 同步对象
        /// </summary>
        protected override object Sync { get { return _params; } }

        /// <summary>
        /// 数量
        /// </summary>
        public override int Count { get { return _params.Count; } }

        /// <summary>
        /// 获取所有ZSettingParam的名称
        /// </summary>
        public override ICollection<string> Params { get { return _params.Keys; } }

        /// <summary>
        /// 获取一个参数对象
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        protected override ParamBase XGetParam(string strParamName)
        {
            return _params.GetOrDefault(strParamName);
        }

        /// <summary>
        /// 添加一个参数对象
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        protected override ParamBase XAddParam(string strParamName)
        {
            SettingParam sp = new SettingParam();
            _params[strParamName] = sp;
            return sp;
        }

        /// <summary>
        /// 删除一个参数对象
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        protected override bool XRemoveParam(string strParamName)
        {
            return _params.Remove(strParamName);
        }
    }
}
