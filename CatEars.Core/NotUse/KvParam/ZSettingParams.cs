﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using CatEars.Core.Collections;

namespace CatEars.Core.Data.KvParam
{
    /// <summary>
    /// 参数组
    /// </summary>
    [DebuggerDisplay("Count={Count}")]
    public abstract class ZSettingParams
    {
        /// <summary>
        /// 参数组
        /// </summary>
        public ZSettingParams()
        {
            this.AddParam(null);
        }

        #region 抽象方法

        /// <summary>
        /// 同步对象
        /// </summary>
        protected abstract object Sync { get; }

        /// <summary>
        /// 获取一个参数对象
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        protected abstract ParamBase XGetParam(string strParamName);

        /// <summary>
        /// 添加一个参数对象
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        protected abstract ParamBase XAddParam(string strParamName);

        /// <summary>
        /// 删除一个参数对象
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        protected abstract bool XRemoveParam(string strParamName);

        #endregion

        /// <summary>
        /// 数量
        /// </summary>
        public abstract int Count { get; }

        /// <summary>
        /// 获取所有ZSettingParam的名称
        /// </summary>
        public abstract ICollection<string> Params { get; }

        /// <summary>
        /// 默认的ZSettingParam
        /// </summary>
        /// <returns>ZSettingParam</returns>
        public ParamBase Default { get { return XGetParam(null); } }

        /// <summary>
        /// 获取一个Group用于读参数
        /// </summary>
        /// <param name="strParamName">strParamName</param>
        /// <returns>ZSettingParam</returns>
        public virtual ParamBase Gr(string strParamName)
        {
            string strParamName0 = (strParamName ?? "").Trim();
            var p = XGetParam(strParamName0);
            if (p == null)
            {
                return SettingParamEmpty.Instance;
            }
            return p;
        }

        /// <summary>
        /// 获取一个Group用于写参数
        /// </summary>
        /// <param name="strParamName">strParamName</param>
        /// <returns>ZSettingParam</returns>
        public virtual ParamBase Gw(string strParamName)
        {
            string strParamName0 = (strParamName ?? "").Trim();
            var p = XGetParam(strParamName0);
            if (p == null)
            {
                p = AddParam(strParamName0);
            }
            return p;
        }

        /// <summary>
        /// 添加一个Param
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        public virtual ParamBase AddParam(string strParamName)
        {
            lock (Sync)
            {
                string strParamName0 = (strParamName ?? "").Trim();
                var p = XGetParam(strParamName0);
                if (p != null)
                {
                    throw new ArgumentException(
                        string.Format("ParamName:“{0}”已存在", strParamName0), "ParamName");
                }
                p = XAddParam(strParamName0);
                return p;
            }
        }

        /// <summary>
        /// 删除一个Group
        /// </summary>
        /// <param name="strParamName"></param>
        /// <returns></returns>
        public virtual bool RemoveParam(string strParamName)
        {
            string strParamName0 = (strParamName ?? "").Trim();
            if (string.IsNullOrEmpty(strParamName0))
            {
                throw new NotSupportedException("默认组不允许删除");
            }
            lock (Sync)
            {
                return XRemoveParam(strParamName0);
            }
        }

        /// <summary>
        /// 进行参数拷贝
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void CopyTo(ZSettingParams to)
        {
            var p = this;
            foreach (string strPName in p.Params)
            {
                var pKv = p.Gr(strPName);
                var pKv2 = to.Gw(strPName);
                var pKeys = pKv.Keys;
                foreach (string strKey in pKeys)
                {
                    pKv2.Set(strKey, pKv[strKey]);
                }
            }
        }

        /// <summary>
        /// 参数对象 ZSettingParam的空实现 无功能
        /// </summary>
        [DebuggerDisplay("SettingParamEmpty")]
        class SettingParamEmpty : ParamBase
        {
            public static SettingParamEmpty Instance = new SettingParamEmpty();

            /// <summary>
            /// 数量
            /// </summary>
            public override int Count { get { return 0; } }

            /// <summary>
            /// 获取已有的Key
            /// </summary>
            public override ICollection<string> Keys
            {
                get { return EmptyArray.StringArray; }
            }

            /// <summary>
            /// 清空
            /// </summary>
            public override void Clear() { }

            /// <summary>
            /// 取值
            /// </summary>
            /// <param name="strKey">key</param>
            /// <param name="strDefault">默认值</param>
            /// <returns>value</returns>
            protected override string XGet(string strKey, string strDefault = null)
            {
                return strDefault;
            }

            /// <summary>
            /// 设置值
            /// </summary>
            /// <param name="strKey">key</param>
            /// <param name="strValue">value</param>
            protected override void XSet(string strKey, string strValue)
            {
            }
        }
    }
}
