﻿
using System;
using System.IO;

namespace CatEars.Core.Data.KvParam
{
    /// <summary>
    /// 参数提供者
    /// </summary>
    public abstract class ZSettingProvider
    {
        /// <summary>
        /// 静态初始化
        /// </summary>
        static ZSettingProvider()
        {
            JsonProvider = new JsonSettingProvider();
            JsonProvider_Safe = new JsonSettingProvider() { SafeMode = true };
        }
        /// <summary>
        /// 默认参数提供者
        /// </summary>
        public static JsonSettingProvider JsonProvider { get; private set; }
        /// <summary>
        /// 默认参数提供者
        /// </summary>
        public static JsonSettingProvider JsonProvider_Safe { get; private set; }

        /// <summary>
        /// 默认参数提供者
        /// </summary>
        public static JsonSettingProvider JsonProvider_Auto
        {
            get
            {
#if DEBUG
                return JsonProvider;
#else
                return JsonProvider_Safe;
#endif
            }
        }

        /// <summary>
        /// 文件后缀名
        /// </summary>
        public string FileExt { get; set; }
        /// <summary>
        /// 文件夹名称(null则使用默认的文件夹)
        /// </summary>
        public string FolderName { get; set; }
        /// <summary>
        /// 设置为true则不抛出异常
        /// </summary>
        public bool SafeMode { get; set; }

        /// <summary>
        /// 默认文件夹
        /// </summary>
        /// <returns></returns>
        protected abstract string GetDefaultFolderName();

        /// <summary>
        /// 对象转Key
        /// </summary>
        /// <param name="objToKey">用objToKey.GetType().FullName作为Key，不能为null</param>
        /// <returns>Key值</returns>
        protected string GetKey(object objToKey)
        {
            if (objToKey is Type)
            {
                return ((Type)objToKey).FullName;
            }
            return objToKey.GetType().FullName;
        }

        /// <summary>
        /// 生成保存文件路径
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="blnCreateFolder">自动创建文件夹</param>
        /// <returns></returns>
        protected string GetTargetFileFullName(string strFileName, bool blnCreateFolder)
        {
            if (string.IsNullOrEmpty(strFileName))
            {
                throw new ArgumentException("文件名不能为空", "strFileName");
            }
            string strBasePath = AppEnv.ExeDirPath;
            string strFolderName = FolderName;
            if (string.IsNullOrEmpty(strFolderName))
            {
                strFolderName = GetDefaultFolderName();
            }
            string strFolder;
            if (Path.IsPathRooted(strFolderName))
            {
                strFolder = strFolderName;
            }
            else
            {
                strFolder = Path.Combine(strBasePath, strFolderName);
            }
            string strFileName0 = strFileName;
            if (!string.IsNullOrEmpty(FileExt))
            {
                if (FileExt[0] == '.') strFileName0 = strFileName0 + FileExt;
                else strFileName0 = strFileName0 + '.' + FileExt;
            }
            if (blnCreateFolder && !Directory.Exists(strFolder))
            {
                Directory.CreateDirectory(strFolder);
            }
            return Path.Combine(strFolder, strFileName0);
        }
    }
}
