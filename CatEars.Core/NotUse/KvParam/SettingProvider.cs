﻿using System.IO;

namespace CatEars.Core.Data.KvParam
{
    /// <summary>
    /// 参数提供者
    /// </summary>
    public abstract class SettingProvider : ZSettingProvider
    {
        /// <summary>
        /// 静态初始化
        /// </summary>
        static SettingProvider()
        {
            CsvProvider = new CsvSettingProvider();
            CsvProvider_Safe = new CsvSettingProvider() { SafeMode = true };
            IniProvider = new IniSettingProvider();
            IniProvider_Safe = new IniSettingProvider() { SafeMode = true };
        }
        /// <summary>
        /// 默认参数提供者
        /// </summary>
        public static CsvSettingProvider CsvProvider { get; private set; }
        /// <summary>
        /// 默认参数提供者
        /// </summary>
        public static CsvSettingProvider CsvProvider_Safe { get; private set; }
        /// <summary>
        /// 默认参数提供者
        /// </summary>
        public static IniSettingProvider IniProvider { get; private set; }
        /// <summary>
        /// 默认参数提供者
        /// </summary>
        public static IniSettingProvider IniProvider_Safe { get; private set; }

        /// <summary>
        /// 默认参数提供者
        /// </summary>
        public static CsvSettingProvider CsvProvider_Auto
        {
            get
            {
#if DEBUG
                return CsvProvider;
#else
                return CsvProvider_Safe;
#endif
            }
        }

        /// <summary>
        /// 创建参数对象的方法
        /// </summary>
        /// <returns></returns>
        public virtual ZSettingParams CreateParam()
        {
            return new SettingParams();
        }
        /// <summary>
        /// 加载参数
        /// </summary>
        /// <param name="strFilePath">存储路径</param>
        /// <returns>加载的参数</returns>
        protected abstract ZSettingParams LoadFromPath(string strFilePath);
        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="strFilePath">存储路径</param>
        /// <param name="p">要保存的参数</param>
        protected abstract void SaveToPath(string strFilePath, ZSettingParams p);

        /// <summary>
        /// 加载参数
        /// </summary>
        /// <param name="strKey">标识符</param>
        /// <returns>加载的参数</returns>
        public ZSettingParams Load(string strKey)
        {
            string strFileName = GetTargetFileFullName(strKey, false);
            ZSettingParams result = null;
            if (File.Exists(strFileName))
            {
                result = LoadFromPath(strFileName);
            }
            if (result == null)
            {
                result = this.CreateParam();
            }
            return result;
        }

        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="strKey">标识符</param>
        /// <param name="p">要保存的参数</param>
        public void Save(string strKey, ZSettingParams p)
        {
            string strFileName = GetTargetFileFullName(strKey, true);
            SaveToPath(strFileName, p);
        }

        #region Object重载
        /// <summary>
        /// 加载参数
        /// </summary>
        /// <param name="objToKey">用objToKey.GetType().FullName作为Key，不能为null</param>
        /// <returns>加载的参数</returns>
        public ZSettingParams Load(object objToKey)
        {
            return Load(GetKey(objToKey));
        }
        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="objToKey">用objToKey.GetType().FullName作为Key，不能为null</param>
        /// <param name="p">要保存的参数</param>
        public void Save(object objToKey, ZSettingParams p)
        {
            Save(GetKey(objToKey), p);
        }
        #endregion
    }
}
