﻿using System.Data;

namespace System.Windows.Forms
{
    /// <summary>
    /// DataGridView扩展类
    /// </summary>
    public static class ExDataGridView
    {
        /// <summary>
        /// 获取DataGridViewRow绑定的DataRow
        /// </summary>
        /// <param name="gridRow">DataGridViewRow</param>
        /// <returns>DataRow</returns>
        public static DataRow GetBindingDataRow(this DataGridViewRow gridRow)
        {
            DataRow dataRow = null;
            DataRowView drv = gridRow?.DataBoundItem as DataRowView;
            if (drv == null)
            {
                return null;
            }
            if (drv.DataView != null && drv.Row != null)
            {
                dataRow = drv.Row;
            }
            return dataRow;
        }
    }
}
