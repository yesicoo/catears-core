﻿
using System.IO;

namespace System.Windows.Forms
{
    /// <summary>
    /// FileDialog扩展类
    /// </summary>
    public static class ExFileDialog
    {
        /// <summary>
        /// 扩展方法 指定对话框的初始路径
        /// </summary>
        /// <param name="dialog">FileDialog对象</param>
        /// <param name="path">要解析的路径</param>
        /// <param name="defaultPath">默认路径</param>
        public static void SetInitialDirectory(
            this FileDialog dialog,
            string path,
            string defaultPath = null)
        {
            if (dialog == null) return;
            try
            {
                #region 直接指定默认路径
                if (string.IsNullOrWhiteSpace(path))
                {
                    dialog.SetDefaultDirectory(defaultPath);
                    return;
                }
                #endregion

                // pathType: 0无效 1:文件夹 2:文件
                int pathType = 0;
                #region 判断路径是文件路径还是文件夹路径
                FileInfo fileInfo = new FileInfo(path);
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                try
                {
                    fileInfo = new FileInfo(path);
                }
                catch { }
                try
                {
                    dirInfo = new DirectoryInfo(path);
                }
                catch { }

                if (dirInfo != null && dirInfo.Exists)
                {
                    pathType = 1;
                }
                else if (fileInfo != null)
                {
                    if (fileInfo.Exists)
                    {
                        pathType = 2;
                    }
                    else if (fileInfo.Directory.Exists)
                    {
                        dirInfo = fileInfo.Directory;
                        pathType = 1;
                    }
                    else
                    {
                        pathType = 0;
                    }
                }
                #endregion

                #region 根据结果设置路径及文件名
                switch (pathType)
                {
                    case 1:
                        dialog.InitialDirectory = dirInfo.FullName;
                        break;
                    case 2:
                        dialog.InitialDirectory = fileInfo.FullName;
                        dialog.FileName = fileInfo.Name;
                        break;
                    default:
                        dialog.InitialDirectory = Application.StartupPath;
                        break;
                }
                #endregion
            }
            catch
            {
                dialog.SetDefaultDirectory(defaultPath);
            }
        }

        /// <summary>
        /// 设置默认路径
        /// </summary>
        /// <param name="dialog">FileDialog对象</param>
        /// <param name="strDefaultPath">默认路径</param>
        private static void SetDefaultDirectory(
            this FileDialog dialog,
            string strDefaultPath)
        {
            if (!string.IsNullOrWhiteSpace(strDefaultPath)
                && Directory.Exists(strDefaultPath))
            {
                dialog.InitialDirectory = strDefaultPath;
            }
            else
            {
                dialog.InitialDirectory = Application.StartupPath;
            }
        }
    }
}
