﻿

namespace System.Windows.Forms
{
    /// <summary>
    /// TextBox
    /// </summary>
    public static class ExTextBoxBase
    {
        /// <summary>
        /// 从文本输入框中获取指定类型的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="text">【NotNull】</param>
        /// <param name="tipText"></param>
        /// <returns>null表示转换类型失败</returns>
        public static Nullable<T> ValueFromText<T>(this TextBoxBase text, string tipText = null) where T : struct
        {
            try
            {
                return (T)Convert.ChangeType(text.Text.Trim(), typeof(T));
            }
            catch
            {
                if (tipText != null)
                {
                    string title = null;
                    var form = text.FindForm();
                    if (form != null)
                    {
                        title = form.Text;
                    }
                    if (string.IsNullOrWhiteSpace(title))
                    {
                        title = "系统提示";
                    }
                    text.Focus();
                    MessageBox.Show(tipText, title);
                }
                return null;
            }
        }
    }
}
