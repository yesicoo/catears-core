﻿using System;

namespace CatEars.Core.Events
{
    /// <summary>
    /// 表示将处理事件的方法
    /// </summary>
    /// <param name="sender">事件源</param>
    /// <param name="e">携带参数</param>
    [Serializable]
    public delegate void CustomEventHandler<T>(object sender, T e);
    /// <summary>
    /// 表示将处理事件的方法
    /// </summary>
    /// <param name="sender">事件源</param>
    /// <param name="e1">携带参数1</param>
    /// <param name="e2">携带参数2</param>
    [Serializable]
    public delegate void CustomEventHandler<T1, T2>(object sender, T1 e1, T2 e2);
    /// <summary>
    /// 表示将处理事件的方法
    /// </summary>
    /// <param name="sender">事件源</param>
    /// <param name="e1">携带参数1</param>
    /// <param name="e2">携带参数2</param>
    /// <param name="e3">携带参数3</param>
    [Serializable]
    public delegate void CustomEventHandler<T1, T2, T3>(object sender, T1 e1, T2 e2, T3 e3);
}
