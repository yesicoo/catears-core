﻿
using System;
using System.Diagnostics;

namespace CatEars.Core.Events
{
    /// <summary>
    /// 基础事件参数类
    /// </summary>
    [DebuggerDisplay("OTag={OTag}")]
    public class CustomEventArgs : EventArgs
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public CustomEventArgs() { }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="tag">关联对象</param>
        public CustomEventArgs(object tag) { OTag = tag; }

        /// <summary>
        /// 关联对象
        /// </summary>
        public virtual object OTag
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 基础事件参数类
    /// </summary>
    [DebuggerDisplay("Tag={Tag}")]
    public class CustomEventArgs<T> : CustomEventArgs
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public CustomEventArgs() { }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="tag">关联对象</param>
        public CustomEventArgs(T tag) { Tag = tag; }

        /// <summary>
        /// 关联对象
        /// </summary>
        public T Tag
        {
            get;
            set;
        }

        /// <summary>
        /// 关联对象
        /// </summary>
        public override object OTag
        {
            get { return Tag; }
            set { base.OTag = Tag; }
        }
    }
}
