﻿
using static CatEars.Core.Properties.Resources;

namespace CatEars.Text.Replace
{
    /// <summary>
    /// 符号替换器（将全角符号全部替换为对应的半角符号）
    /// </summary>
    public class SymbolReplace : MultiReplace
    {
        /// <summary>
        /// 单例对象
        /// </summary>
        public static SymbolReplace Instance;

        /// <summary>
        /// 替换后可能的符号集合
        /// </summary>
        public static string TargetSymbolColl;

        /// <summary>
        /// 静态初始化
        /// </summary>
        static SymbolReplace()
        {
            Instance = new SymbolReplace();
            TargetSymbolColl = @",.:;'""~`^_-@?!&|%$#=+*/\()<>[]{}";
        }

        /// <summary>
        /// 静态替换方法
        /// </summary>
        /// <param name="strValue">要转换的字符串</param>
        /// <returns>转换后的字符串</returns>
        public static string SReplace(string strValue)
        {
            return SymbolReplace.Instance.Replace(strValue);
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        public SymbolReplace()
        {
            //默认添加Tab转空格
            Init_AddReplace("\t", " ");
            Init_FromText(String_SymbolConv, '\t');
        }
    }
}
