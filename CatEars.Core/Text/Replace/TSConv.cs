﻿
using static CatEars.Core.Properties.Resources;

namespace CatEars.Text.Replace
{
    /// <summary>
    /// 繁简转换
    /// </summary>
    public static class TSConv
    {
        /// <summary>
        /// 繁体转简体
        /// </summary>
        class T2SConv : MultiReplace
        {
            public static T2SConv Instance;

            static T2SConv()
            {
                Instance = new T2SConv();
                Instance.Init(String_T2S, null);
            }
        }

        /// <summary>
        /// 简体转繁体
        /// </summary>
        class S2TConv : MultiReplace
        {
            public static S2TConv Instance;

            static S2TConv()
            {
                Instance = new S2TConv();
                Instance.Init(String_S2T, null);
            }
        }

        /// <summary>
        /// 繁体转简体
        /// </summary>
        /// <param name="strValue">要转换的字符串</param>
        /// <returns>转换后的字符串</returns>
        public static string T2S(string strValue)
        {
            return T2SConv.Instance.Replace(strValue);
        }
        /// <summary>
        /// 简体转繁体
        /// </summary>
        /// <param name="strValue">要转换的字符串</param>
        /// <returns>转换后的字符串</returns>
        public static string S2T(string strValue)
        {
            return S2TConv.Instance.Replace(strValue);
        }

        /// <summary>
        /// 初始化词库
        /// </summary>
        /// <param name="convMapping">可替换映射词库的文件内容</param>
        /// <param name="stopMapping">不可替换映射词库的文件内容</param>
        public static void InitT2S(string convMapping, string stopMapping)
        {
            T2SConv.Instance.Init(convMapping, stopMapping);
        }
        /// <summary>
        /// 初始化词库
        /// </summary>
        /// <param name="convMapping">可替换映射词库的文件内容</param>
        /// <param name="stopMapping">不可替换映射词库的文件内容</param>
        public static void InitS2T(string convMapping, string stopMapping)
        {
            S2TConv.Instance.Init(convMapping, stopMapping);
        }
    }
}
