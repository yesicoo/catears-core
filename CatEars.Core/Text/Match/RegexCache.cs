﻿
using System.Diagnostics;
using System.Text.RegularExpressions;
using CatEars.Core.Collections;

namespace CatEars.Text.Match
{
    /// <summary>
    /// 正则表达式缓存
    /// </summary>
    [DebuggerDisplay("CacheCount={_cache.Count}")]
    public class RegexCache
    {
        /// <summary>
        /// 全局对象
        /// </summary>
        public static RegexCache Instance = new RegexCache();

        /// <summary>
        /// 缓存 Key由RegexOptions和正则字符串构成
        /// </summary>
        DictEx<string, Regex> _cache = new DictEx<string, Regex>();

        /// <summary>
        /// 初始化Regex对象
        /// </summary>
        /// <param name="regex">regex</param>
        /// <returns>Regex对象</returns>
        static Regex CreateRegex(object[] regex)
        {
            string strRegex = (string)(regex[0]);
            //强制加上RegexOptions.Compiled属性
            if (regex.Length > 1)
            {
                RegexOptions opt = (RegexOptions)regex[1] | RegexOptions.Compiled;
                return new Regex(strRegex, opt);
            }
            else
            {
                //由于是缓存 默认使用RegexOptions.Compiled以提高效率
                return new Regex(strRegex, RegexOptions.Compiled);
            }
        }

        /// <summary>
        /// 创建并缓存一个Regex对象
        /// </summary>
        /// <param name="strRegex">strRegex</param>
        /// <returns>Regex对象</returns>
        public Regex GetRegex(string strRegex)
        {
            int intFlag = (int)RegexOptions.Compiled;
            return _cache.GetOrAddValue(intFlag + "_" + strRegex, CreateRegex, strRegex);
        }

        /// <summary>
        /// 创建并缓存一个Regex对象
        /// </summary>
        /// <param name="strRegex">strRegex</param>
        /// <param name="option">option</param>
        /// <returns>Regex对象</returns>
        public Regex GetRegex(string strRegex, RegexOptions option)
        {
            int intFlag = (int)(option | RegexOptions.Compiled);
            return _cache.GetOrAddValue(intFlag + "_" + strRegex, CreateRegex, strRegex);
        }

        /// <summary>
        /// 移除缓存
        /// </summary>
        /// <param name="strRegex">strRegex</param>
        /// <returns>是否成功</returns>
        public bool Remove(string strRegex)
        {
            int intFlag = (int)RegexOptions.Compiled;
            return _cache.InnerDict.Remove(intFlag + "_" + strRegex);
        }

        /// <summary>
        /// 移除缓存
        /// </summary>
        /// <param name="strRegex">strRegex</param>
        /// <param name="option">option</param>
        /// <returns>是否成功</returns>
        public bool Remove(string strRegex, RegexOptions option)
        {
            int intFlag = (int)(option | RegexOptions.Compiled);
            return _cache.InnerDict.Remove(intFlag + "_" + strRegex);
        }

        /// <summary>
        /// 清除缓存
        /// </summary>
        public void ClearCache()
        {
            var old = _cache;
            _cache = new DictEx<string, Regex>();
            old.InnerDict.Clear();
        }
    }
}
