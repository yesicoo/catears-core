﻿
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using CatEars.Core.Collections;

namespace CatEars.Text.Match
{
    /// <summary>
    /// 常用正则表达式
    /// </summary>
    public static class RegexEx
    {
        /// <summary>
        /// 缓存
        /// </summary>
        static RegexCache _cache = new RegexCache();

        #region 正则转义

        static HashSet<char> _regexEscape = new HashSet<char>("^$()[]{}.*+?\\|");

        /// <summary>
        /// 正则表达式转义
        /// </summary>
        /// <param name="strValue">一般字符串</param>
        /// <returns></returns>
        public static string DoEscape(string strValue)
        {
            if (string.IsNullOrEmpty(strValue))
            {
                return "";
            }
            StringBuilder strB = new StringBuilder();
            foreach (var chr in strValue)
            {
                if (_regexEscape.Contains(chr))
                {
                    strB.Append('\\');
                }
                strB.Append(chr);
            }
            if (strB.Length != strValue.Length) return strB.ToString();
            else return strValue;
        }

        #endregion

        #region 中文判断
        /// <summary>
        /// 判断是否含有中文
        /// </summary>
        /// <param name="str">str</param>
        /// <returns>返回值</returns>
        public static bool IsChinese(string str)
        {
            //return GetRegex(@"[\u4e00-\u9fa5]+").IsMatch(str);
            return _cache.GetRegex(@"[\u4e00-\u9fff]+").IsMatch(str);
        }
        /// <summary>
        /// 判断是否全是中文
        /// </summary>
        /// <param name="str">str</param>
        /// <returns>返回值</returns>
        public static bool IsAllChinese(string str)
        {
            //return GetRegex(@"^[\u4e00-\u9fa5]+$").IsMatch(str);
            return _cache.GetRegex(@"^[\u4e00-\u9fff]+$").IsMatch(str);
        }
        #endregion

        #region 字段提取器

        /// <summary>
        /// 匹配正则表达式中的字段
        /// </summary>
        /// <param name="regex">正则</param>
        /// <param name="strText">输入字符串</param>
        /// <returns>结果</returns>
        public static string ExtractValue(Regex regex, string strText)
        {
            var match = regex.Match(strText);
            if (match == null || !match.Success)
            {
                return null;
            }
            if (match.Groups.Count > 1)
            {
                return match.Groups[1].Value;
            }
            else
            {
                return match.Value;
            }
        }

        /// <summary>
        /// 匹配正则表达式中的字段
        /// </summary>
        /// <param name="regex">正则</param>
        /// <param name="strText">输入字符串</param>
        /// <returns>结果</returns>
        public static string[] ExtractField(Regex regex, string strText)
        {
            var match = regex.Match(strText);
            if (match == null || !match.Success)
            {
                return EmptyArray.StringArray;
            }
            string[] result = new string[match.Groups.Count];
            for (int i = 0; i < result.Length; i++)
            {
                string strValue = match.Groups[i].Value;
                result[i] = strValue;
            }
            return result;
        }

        /// <summary>
        /// 匹配正则表达式中的字段
        /// </summary>
        /// <param name="regex">正则</param>
        /// <param name="strText">输入字符串</param>
        /// <param name="strFields">获取字段</param>
        /// <returns>结果</returns>
        public static string[] ExtractField(Regex regex, string strText, string[] strFields)
        {
            var match = regex.Match(strText);
            if (match == null || !match.Success)
            {
                return EmptyArray.StringArray;
            }
            string[] result = new string[strFields.Length];
            for (int i = 0; i < strFields.Length; i++)
            {
                string strField = strFields[i];
                string strValue = match.Groups[strField].Value;
                result[i] = strValue;
            }
            return result;
        }

        /// <summary>
        /// 匹配正则表达式中的字段
        /// </summary>
        /// <param name="regex">正则</param>
        /// <param name="strText">输入字符串</param>
        /// <returns>结果</returns>
        public static IEnumerable<string[]> ExtractFields(Regex regex, string strText)
        {
            var matchs = regex.Matches(strText);
            if (matchs == null || matchs.Count == 0)
            {
                yield break;
            }
            foreach (System.Text.RegularExpressions.Match match in matchs)
            {
                if (!match.Success)
                {
                    continue;
                }
                string[] result = new string[match.Groups.Count];
                for (int i = 0; i < match.Groups.Count; i++)
                {
                    string strValue = match.Groups[i].Value;
                    result[i] = strValue;
                }
                yield return result;
            }
        }

        /// <summary>
        /// 匹配正则表达式中的字段
        /// </summary>
        /// <param name="regex">正则</param>
        /// <param name="strText">输入字符串</param>
        /// <param name="strFields">获取字段</param>
        /// <returns>结果</returns>
        public static IEnumerable<string[]> ExtractFields(Regex regex, string strText, string[] strFields)
        {
            var matchs = regex.Matches(strText);
            if (matchs == null || matchs.Count == 0)
            {
                yield break;
            }
            foreach (System.Text.RegularExpressions.Match match in matchs)
            {
                if (!match.Success)
                {
                    continue;
                }
                string[] result = new string[strFields.Length];
                for (int i = 0; i < strFields.Length; i++)
                {
                    string strField = strFields[i];
                    string strValue = match.Groups[strField].Value;
                    result[i] = strValue;
                }
                yield return result;
            }
        }

        #endregion
    }
}
