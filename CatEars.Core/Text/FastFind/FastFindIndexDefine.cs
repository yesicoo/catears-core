﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CatEars.Text.FastFind
{
    /// <summary>
    /// 快速搜索子串 索引类型
    /// </summary>
    [Flags]
    public enum FastFindIndexMode
    {
        /// <summary>
        /// 不可用
        /// </summary>
        None,
        /// <summary>
        /// 顺序索引
        /// </summary>
        Asc = 1,
        /// <summary>
        /// 反序索引
        /// </summary>
        Desc = 2,
    }

    /// <summary>
    /// FastFindSubStrResult的属性取值
    /// </summary>
    public enum FastFindResultIndexMode
    {
        /// <summary>
        /// 相对于开头
        /// </summary>
        Start,
        /// <summary>
        /// 相对于结尾
        /// </summary>
        End
    }

    /// <summary>
    /// 结果排序模式
    /// </summary>
    public enum FastFindResultSortMode
    {
        /// <summary>
        /// 不排序
        /// </summary>
        None,
        /// <summary>
        /// 只按长度
        /// </summary>
        ValueLength,
        /// <summary>
        /// 取长度最长的一个
        /// </summary>
        MaxValueLength,
        /// <summary>
        /// 取长度最长的一个
        /// </summary>
        MaxValueLengthEachIndex,
        /// <summary>
        /// 长度优先，位置ASC靠前Desc靠后
        /// </summary>
        ValueLength_MatchIndex,
        /// <summary>
        /// 位置ASC靠前Desc靠后,长度优先
        /// </summary>
        MatchIndex_ValueLength,
        /// <summary>
        /// 长度优先，位置ASC靠后Desc靠前
        /// </summary>
        ValueLength_MatchIndex2,
        /// <summary>
        /// 位置ASC靠后Desc靠前,长度优先
        /// </summary>
        MatchIndex2_ValueLength,
    }

    /// <summary>
    /// 结果中的Tag信息
    /// </summary>
    public enum FastFindResultTagMode
    {
        /// <summary>
        /// 正常
        /// </summary>
        Many,
        /// <summary>
        /// 单个Tag
        /// </summary>
        Single,
        /// <summary>
        /// 无Tag
        /// </summary>
        None,
    }

    /// <summary>
    /// 字符 - ID
    /// </summary>
    struct CharItem
    {
        public int ID;
        public int Index;
        public int Length;

        public override string ToString()
        {
            return string.Format("ID={0} Index={1} Length={2}", ID, Index, Length);
        }
    }

    /// <summary>
    /// 字符串 带 Tag
    /// </summary>
    [DebuggerDisplay("Value={Value} Tag={Tag}")]
    struct StringItem
    {
        public string Value;
        public object Tag;
    }

    /// <summary>
    /// 快速搜索子串结果对象
    /// </summary>
    [DebuggerDisplay("{Value} MatchIndex={MatchIndex} Tags.Count={Tags.Count}")]
    public class FastFindSubStrResult
    {
#pragma warning disable 1591
        public FastFindSubStrResult(
            string strValue,
            ICollection<object> tags,
            int intMatchIndex)
        {
            Value = strValue;
            Tags = tags;
            MatchIndex = intMatchIndex;
        }

        public string Value { get; internal set; }

        public ICollection<object> Tags { get; internal set; }

        public int MatchIndex { get; internal set; }
    }
}
