﻿
using System;
using System.Data;

namespace CatEars.Core.Data
{
    /// <summary>
    /// 数据读取接口
    /// </summary>
    public interface ICommonReader
    {
        /// <summary>
        /// 下一行
        /// </summary>
        /// <returns>是否有下一行</returns>
        bool Next();

        /// <summary>
        /// 取值方法
        /// </summary>
        /// <param name="intIndex">字段索引</param>
        /// <returns>值</returns>
        object this[int intIndex] { get; }

        /// <summary>
        /// 取值方法
        /// </summary>
        /// <param name="strFieldName">字段名</param>
        /// <returns>值</returns>
        object this[string strFieldName] { get; }
    }

    /// <summary>
    /// 用于DataTable的数据读取接口
    /// </summary>
    public class Reader4Dt : ICommonReader
    {
        /// <summary>
        /// 行索引
        /// </summary>
        int m_intRowIndex = -1;
        /// <summary>
        /// 数据表
        /// </summary>
        DataTable m_dt;

        /// <summary>
        /// 用于DataTable的数据读取接口
        /// </summary>
        /// <param name="dt">dt</param>
        public Reader4Dt(DataTable dt)
        {
            m_dt = dt;
        }

        /// <summary>
        /// 下一行
        /// </summary>
        /// <returns>是否有下一行</returns>
        public bool Next()
        {
            m_intRowIndex++;
            return m_dt.Rows.Count > m_intRowIndex;
        }

        /// <summary>
        /// 取值方法
        /// </summary>
        /// <param name="intIndex">字段索引</param>
        /// <returns>值</returns>
        public object this[int intIndex]
        {
            get
            {
                return m_dt.Rows[m_intRowIndex][intIndex];
            }
        }

        /// <summary>
        /// 取值方法
        /// </summary>
        /// <param name="strFieldName">字段名</param>
        /// <returns>值</returns>
        public object this[string strFieldName]
        {
            get
            {
                return m_dt.Rows[m_intRowIndex][strFieldName];
            }
        }
    }

    /// <summary>
    /// 用于DataRow的数据读取接口
    /// </summary>
    public class Reader4Dr : ICommonReader
    {
        /// <summary>
        /// 数据表
        /// </summary>
        DataRow m_dr;

        /// <summary>
        /// 用于DataTable的数据读取接口
        /// </summary>
        /// <param name="dr">dr</param>
        public Reader4Dr(DataRow dr)
        {
            m_dr = dr;
        }

        /// <summary>
        /// 下一行
        /// </summary>
        /// <returns>是否有下一行</returns>
        public bool Next()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 取值方法
        /// </summary>
        /// <param name="intIndex">字段索引</param>
        /// <returns>值</returns>
        public object this[int intIndex]
        {
            get
            {
                return m_dr[intIndex];
            }
        }

        /// <summary>
        /// 取值方法
        /// </summary>
        /// <param name="strFieldName">字段名</param>
        /// <returns>值</returns>
        public object this[string strFieldName]
        {
            get
            {
                return m_dr[strFieldName];
            }
        }
    }

    /// <summary>
    /// 用于IDataReader的数据读取接口
    /// </summary>
    public class Reader4DBReader : ICommonReader
    {
        /// <summary>
        /// 数据表
        /// </summary>
        System.Data.IDataReader m_dr;

        /// <summary>
        /// 用于IDataReader的数据读取接口
        /// </summary>
        /// <param name="dr">dr</param>
        public Reader4DBReader(System.Data.IDataReader dr)
        {
            m_dr = dr;
        }

        /// <summary>
        /// 下一行
        /// </summary>
        /// <returns>是否有下一行</returns>
        public bool Next()
        {
            return m_dr.Read();
        }

        /// <summary>
        /// 取值方法
        /// </summary>
        /// <param name="intIndex">字段索引</param>
        /// <returns>值</returns>
        public object this[int intIndex]
        {
            get
            {
                return m_dr[intIndex];
            }
        }

        /// <summary>
        /// 取值方法
        /// </summary>
        /// <param name="strFieldName">字段名</param>
        /// <returns>值</returns>
        public object this[string strFieldName]
        {
            get
            {
                return m_dr[strFieldName];
            }
        }
    }
}
