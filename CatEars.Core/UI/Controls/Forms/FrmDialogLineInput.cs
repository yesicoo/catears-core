﻿
using System;
using System.Windows.Forms;

namespace CatEars.Core.UI.Controls.Forms
{
    /// <summary>
    /// 单行文本输入框
    /// </summary>
    public partial class FrmDialogLineInput : Form
    {
        /// <summary>
        /// 静态方法 显示输入框
        /// </summary>
        /// <param name="title">窗体标题</param>
        /// <param name="text">提示文本</param>
        /// <param name="value">默认值</param>
        /// <param name="allowEmptyInput">是否允许空输入</param>
        /// <param name="hideText">是否隐藏文本(密码)</param>
        /// <param name="allowUserClose">是否允许用户手动关闭窗体</param>
        /// <returns>输入内容，用户关闭窗体返回null</returns>
        public static string ShowLineInput(
            string title,
            string text = null,
            string value = null,
            bool allowEmptyInput = false,
            bool hideText = false,
            bool allowUserClose = true)
        {
            FrmDialogLineInput dialog =
                new FrmDialogLineInput(title, text, value);
            dialog.AllowEmptyText = allowEmptyInput;
            dialog.HideText = hideText;
            dialog.AllowUserClose = allowUserClose;
            DialogResult dr = dialog.ShowDialog();
            if (dr == DialogResult.OK)
            {
                return dialog.TextValue;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 输入的字符串值
        /// </summary>
        public string TextValue
        {
            get
            {
                return txtValue.Text;
            }
            set
            {
                txtValue.Text = value;
            }
        }

        /// <summary>
        /// 是否允许空输入
        /// </summary>
        public bool AllowEmptyText
        {
            get;
            set;
        }

        /// <summary>
        /// 是否允许用户关闭窗体
        /// </summary>
        public bool AllowUserClose
        {
            get;
            set;
        }

        /// <summary>
        /// 是否隐藏文本(密码)
        /// </summary>
        public bool HideText
        {
            get
            {
                return (txtValue.PasswordChar == '*');
            }
            set
            {
                if (value)
                {
                    txtValue.PasswordChar = '*';
                }
                else
                {
                    txtValue.PasswordChar = ' ';
                }
            }
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        public FrmDialogLineInput()
        {
            InitializeComponent();
            AllowEmptyText = false;
            AllowUserClose = true;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="title">窗体标题</param>
        /// <param name="text">提示文本</param>
        /// <param name="value">默认值</param>
        public FrmDialogLineInput(string title,
            string text = null,
            string value = null)
            : this()
        {
            this.Text = title;
            if (text != null)
            {
                lblText.Text = text;
            }
            else
            {
                lblText.Text = "";
            }
            if (value != null)
            {
                txtValue.Text = value;
            }
        }

        /// <summary>
        /// 设置自动补全字符串列表
        /// </summary>
        /// <param name="strAutoArr">自动补全字符串数组</param>
        /// <param name="autoCompleteMode">自动补全模式</param>
        public void SetAutoCompleteList(
            string[] strAutoArr,
            AutoCompleteMode autoCompleteMode = AutoCompleteMode.Append)
        {
            txtValue.AutoCompleteMode = autoCompleteMode;
            txtValue.AutoCompleteCustomSource.AddRange(strAutoArr);
        }

        /// <summary>
        /// 完成按钮按下
        /// </summary>
        /// <param name="sender">事件源</param>
        /// <param name="e">事件数据</param>
        private void btnFinish_Click(object sender, EventArgs e)
        {
            if (!AllowEmptyText && txtValue.Text.Length == 0)
            {
                txtValue.Focus();
                return;
            }
            bool blnTmp = AllowUserClose;
            this.DialogResult = DialogResult.OK;
            this.Close();
            AllowUserClose = blnTmp;
        }

        /// <summary>
        /// 限制关闭
        /// </summary>
        /// <param name="sender">事件源</param>
        /// <param name="e">事件数据</param>
        private void DialogLineInput_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!AllowUserClose)
            {
                e.Cancel = true;
            }
        }
    }
}
