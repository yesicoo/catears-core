﻿
namespace CatEars.Core.UI.Controls.Forms
{
    partial class CtlLunarDateTimeConvertor
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDetial = new System.Windows.Forms.Label();
            this.grpOperation = new System.Windows.Forms.GroupBox();
            this.cboDay = new System.Windows.Forms.ComboBox();
            this.cboMonth = new System.Windows.Forms.ComboBox();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.rbtnToGregorian = new System.Windows.Forms.RadioButton();
            this.rbtnToLunar = new System.Windows.Forms.RadioButton();
            this.btnHelp = new System.Windows.Forms.Button();
            this.grpView = new System.Windows.Forms.GroupBox();
            this.lblTip = new System.Windows.Forms.Label();
            this.grpOperation.SuspendLayout();
            this.grpView.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDetial
            // 
            this.lblDetial.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDetial.Location = new System.Drawing.Point(6, 25);
            this.lblDetial.Name = "lblDetial";
            this.lblDetial.Size = new System.Drawing.Size(166, 162);
            this.lblDetial.TabIndex = 0;
            this.lblDetial.Text = "显示区";
            this.lblDetial.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDetial.DoubleClick += new System.EventHandler(this.lblDetial_DoubleClick);
            // 
            // grpOperation
            // 
            this.grpOperation.Controls.Add(this.cboDay);
            this.grpOperation.Controls.Add(this.cboMonth);
            this.grpOperation.Controls.Add(this.cboYear);
            this.grpOperation.Controls.Add(this.rbtnToGregorian);
            this.grpOperation.Controls.Add(this.rbtnToLunar);
            this.grpOperation.Location = new System.Drawing.Point(3, 3);
            this.grpOperation.Name = "grpOperation";
            this.grpOperation.Size = new System.Drawing.Size(150, 195);
            this.grpOperation.TabIndex = 0;
            this.grpOperation.TabStop = false;
            this.grpOperation.Text = "操作区";
            // 
            // cboDay
            // 
            this.cboDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDay.FormattingEnabled = true;
            this.cboDay.Location = new System.Drawing.Point(20, 160);
            this.cboDay.Name = "cboDay";
            this.cboDay.Size = new System.Drawing.Size(110, 20);
            this.cboDay.TabIndex = 4;
            this.cboDay.SelectedIndexChanged += new System.EventHandler(this.cbo_Day_SelectedIndexChanged);
            // 
            // cboMonth
            // 
            this.cboMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMonth.FormattingEnabled = true;
            this.cboMonth.Location = new System.Drawing.Point(20, 125);
            this.cboMonth.Name = "cboMonth";
            this.cboMonth.Size = new System.Drawing.Size(110, 20);
            this.cboMonth.TabIndex = 3;
            this.cboMonth.SelectedIndexChanged += new System.EventHandler(this.cbo_Month_SelectedIndexChanged);
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(20, 90);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(110, 20);
            this.cboYear.TabIndex = 2;
            this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cbo_Year_SelectedIndexChanged);
            // 
            // rbtnToGregorian
            // 
            this.rbtnToGregorian.AutoSize = true;
            this.rbtnToGregorian.Location = new System.Drawing.Point(28, 55);
            this.rbtnToGregorian.Name = "rbtnToGregorian";
            this.rbtnToGregorian.Size = new System.Drawing.Size(83, 16);
            this.rbtnToGregorian.TabIndex = 1;
            this.rbtnToGregorian.Text = "农历转公历";
            this.rbtnToGregorian.UseVisualStyleBackColor = true;
            this.rbtnToGregorian.CheckedChanged += new System.EventHandler(this.rbtn_CheckedChanged);
            // 
            // rbtnToLunar
            // 
            this.rbtnToLunar.AutoSize = true;
            this.rbtnToLunar.Location = new System.Drawing.Point(28, 25);
            this.rbtnToLunar.Name = "rbtnToLunar";
            this.rbtnToLunar.Size = new System.Drawing.Size(83, 16);
            this.rbtnToLunar.TabIndex = 0;
            this.rbtnToLunar.Text = "公历转农历";
            this.rbtnToLunar.UseVisualStyleBackColor = true;
            this.rbtnToLunar.CheckedChanged += new System.EventHandler(this.rbtn_CheckedChanged);
            // 
            // btnHelp
            // 
            this.btnHelp.Location = new System.Drawing.Point(257, 199);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(80, 23);
            this.btnHelp.TabIndex = 3;
            this.btnHelp.Text = "帮 助";
            this.btnHelp.UseVisualStyleBackColor = true;
            this.btnHelp.Visible = false;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // grpView
            // 
            this.grpView.Controls.Add(this.lblDetial);
            this.grpView.Location = new System.Drawing.Point(159, 3);
            this.grpView.Name = "grpView";
            this.grpView.Size = new System.Drawing.Size(178, 195);
            this.grpView.TabIndex = 1;
            this.grpView.TabStop = false;
            this.grpView.Text = "显示区";
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.ForeColor = System.Drawing.Color.DarkRed;
            this.lblTip.Location = new System.Drawing.Point(21, 204);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(125, 12);
            this.lblTip.TabIndex = 2;
            this.lblTip.Text = "双击显示区可复制文本";
            // 
            // CtlLunarDateTimeConvertor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.grpView);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.grpOperation);
            this.MinimumSize = new System.Drawing.Size(340, 225);
            this.Name = "CtlLunarDateTimeConvertor";
            this.Size = new System.Drawing.Size(340, 225);
            this.grpOperation.ResumeLayout(false);
            this.grpOperation.PerformLayout();
            this.grpView.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDetial;
        private System.Windows.Forms.GroupBox grpOperation;
        private System.Windows.Forms.ComboBox cboDay;
        private System.Windows.Forms.ComboBox cboMonth;
        private System.Windows.Forms.ComboBox cboYear;
        private System.Windows.Forms.RadioButton rbtnToGregorian;
        private System.Windows.Forms.RadioButton rbtnToLunar;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.GroupBox grpView;
        private System.Windows.Forms.Label lblTip;

    }
}
