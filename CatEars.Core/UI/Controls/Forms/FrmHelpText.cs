﻿
using System.Windows.Forms;

namespace CatEars.Core.UI.Controls.Forms
{
    /// <summary>
    /// 帮助显示窗体
    /// 支持显示文本，富文本
    /// </summary>
    public partial class FrmHelpText : Form
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FrmHelpText()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 获取或设置内容文本
        /// </summary>
        public string InnerText
        {
            get
            {
                return rtxtHelp.Text;
            }
            set
            {
                rtxtHelp.Text = value;
            }
        }

        /// <summary>
        /// 获取或设置RTF格式的内容文本
        /// </summary>
        public string InnerRtfText
        {
            get
            {
                return rtxtHelp.Rtf;
            }
            set
            {
                rtxtHelp.Rtf = value;
            }
        }
    }
}
