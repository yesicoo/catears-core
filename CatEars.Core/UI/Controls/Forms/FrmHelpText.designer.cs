﻿
namespace CatEars.Core.UI.Controls.Forms
{
    partial class FrmHelpText
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtxtHelp = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // rtxtHelp
            // 
            this.rtxtHelp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtHelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtHelp.Location = new System.Drawing.Point(0, 0);
            this.rtxtHelp.Name = "rtxtHelp";
            this.rtxtHelp.ReadOnly = true;
            this.rtxtHelp.Size = new System.Drawing.Size(734, 462);
            this.rtxtHelp.TabIndex = 0;
            this.rtxtHelp.Text = "";
            // 
            // FrmHelpText
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 462);
            this.Controls.Add(this.rtxtHelp);
            this.MinimumSize = new System.Drawing.Size(300, 200);
            this.Name = "FrmHelpText";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "帮助";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtxtHelp;

    }
}