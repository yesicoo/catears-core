﻿
using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CatEars.Core.Define;

namespace CatEars.Core.UI.Controls.Forms
{
    /// <summary>
    /// 公历农历转换界面
    /// </summary>
    public partial class CtlLunarDateTimeConvertor : UserControl
    {
        /// <summary>
        /// 当前转换状态 1公历转农历 2农历转公历
        /// </summary>
        private int m_state = 0;

        /// <summary>
        /// 农历日期对象
        /// </summary>
        public LunarDateTime LunarDateTime
        {
            get;
            set;
        }

        /// <summary>
        /// 设置帮助按钮可见性
        /// </summary>
        public bool HelpButtonVisible
        {
            get
            {
                return btnHelp.Visible;
            }
            set
            {
                btnHelp.Visible = value;
            }
        }

        /// <summary>
        /// 选中项正在修改标识，防止ComboSelectedIndexChanged事件触发
        /// </summary>
        private bool m_changeMode = false;

        /// <summary>
        /// 年份格式化字符串
        /// </summary>
        private string m_yearFormat = "{0,4}年";
        /// <summary>
        /// 月份格式化字符串
        /// </summary>
        private string m_monthFormat = "{0,3} 月";
        /// <summary>
        /// 日期格式化字符串
        /// </summary>
        private string m_dayFormat = "{0,3} 日";

        /// <summary>
        /// 构造函数
        /// </summary>
        public CtlLunarDateTimeConvertor()
        {
            InitializeComponent();
            LunarDateTime = new LunarDateTime(DateTime.Now);
            rbtnToLunar.Checked = true;

            this.Tag = "公历农历转换";
        }

        /// <summary>
        /// RadioButton选中项修改
        /// </summary>
        /// <param name="sender">事件源</param>
        /// <param name="e">事件参数</param>
        private void rbtn_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnToLunar.Checked == true)
            {
                //公历转农历
                m_changeMode = true;
                m_state = 1;
                SetYears();

                cboYear.SelectedItem = string.Format(
                    m_yearFormat, LunarDateTime.Gregorian.Year);
                cboMonth.SelectedItem = string.Format(
                    m_monthFormat, LunarDateTime.Gregorian.Month);
                cboDay.SelectedItem = string.Format(
                    m_dayFormat, LunarDateTime.Gregorian.Day);

                m_changeMode = false;
            }
            else if (rbtnToGregorian.Checked == true)
            {
                //农历转公历
                m_changeMode = true;
                m_state = 2;
                SetYears();

                cboYear.SelectedItem = string.Format(m_yearFormat, LunarDateTime.Year);
                cboMonth.SelectedIndex = LunarDateTime.Month - 1;
                cboDay.SelectedIndex = LunarDateTime.Day - 1;

                m_changeMode = false;
            }
            else
            {
                m_state = 0;
                cboYear.Items.Clear();
                cboMonth.Items.Clear();
                cboDay.Items.Clear();
            }
        }

        /// <summary>
        /// 年份选中项修改
        /// </summary>
        /// <param name="sender">事件源</param>
        /// <param name="e">事件参数</param>
        private void cbo_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetMonths();
            if (m_changeMode == false)
            {
                cboMonth.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 月份选中项修改
        /// </summary>
        /// <param name="sender">事件源</param>
        /// <param name="e">事件参数</param>
        private void cbo_Month_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDays();
            if (m_changeMode == false)
            {
                cboDay.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// 日期选中项修改
        /// </summary>
        /// <param name="sender">事件源</param>
        /// <param name="e">事件参数</param>
        private void cbo_Day_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_state == 1)
            {
                LunarDateTime = new LunarDateTime(
                    true,
                    Convert.ToInt32(
                        cboYear.SelectedItem.ToString().Replace("年", "").Trim()),
                    Convert.ToInt32(
                        cboMonth.SelectedItem.ToString().Replace("月", "").Trim()),
                    Convert.ToInt32(
                        cboDay.SelectedItem.ToString().Replace("日", "").Trim())
                );
            }
            else if (m_state == 2)
            {
                LunarDateTime = new LunarDateTime(
                    false,
                    Convert.ToInt32(cboYear.SelectedItem.ToString()
                        .Replace("年", "").Trim()),
                    cboMonth.SelectedIndex + 1,
                    cboDay.SelectedIndex + 1
                );
            }
            ShowDetial();
        }

        /// <summary>
        /// 设置年份下拉项
        /// </summary>
        private void SetYears()
        {
            cboYear.Items.Clear();
            if (m_state == 1)
            {
                for (int i = LunarDateTime.MinSupportedDateTime.Year;
                    i <= LunarDateTime.MaxSupportedDateTime.Year;
                    i++)
                {
                    cboYear.Items.Add(string.Format(m_yearFormat, i));
                }
            }
            else if (m_state == 2)
            {
                for (int i = LunarDateTime.MinSupportedDateTime.Year;
                    i < LunarDateTime.MaxSupportedDateTime.Year;
                    i++)
                {
                    cboYear.Items.Add(string.Format(m_yearFormat, i));
                }
            }
        }

        /// <summary>
        /// 设置月份下拉项
        /// </summary>
        private void SetMonths()
        {
            int selectYear = Convert.ToInt32(cboYear.SelectedItem
                .ToString().Replace("年", "").Trim());
            cboMonth.Items.Clear();
            if (m_state == 1)
            {
                int start = 1, end = 12;
                if (selectYear == LunarDateTime.MinSupportedDateTime.Year)
                {
                    start = LunarDateTime.MinSupportedDateTime.Month;
                }
                else if (selectYear == LunarDateTime.MaxSupportedDateTime.Year)
                {
                    end = LunarDateTime.MaxSupportedDateTime.Month;
                }
                for (int i = start; i <= end; i++)
                {
                    cboMonth.Items.Add(string.Format(m_monthFormat, i));
                }
            }
            else if (m_state == 2)
            {
                int start = 1;
                int end = LunarDateTime.GetMonthsInYear(selectYear);
                int leap = LunarDateTime.GetLeapMonth(selectYear);
                for (int i = start; i <= end; i++)
                {
                    cboMonth.Items.Add(
                        " " + LunarDateTime.GetLunarMonthName(i, leap) + "月");
                }
            }
        }

        /// <summary>
        /// 设置日期下拉项
        /// </summary>
        private void SetDays()
        {
            int selectYear = Convert.ToInt32(
                cboYear.SelectedItem.ToString().Replace("年", "").Trim());
            cboDay.Items.Clear();
            if (m_state == 1)
            {
                int selectMonth = Convert.ToInt32(
                    cboMonth.SelectedItem.ToString().Replace("月", "").Trim());
                int start = 1, end;
                DateTime myDateTime = new DateTime(selectYear, selectMonth, 1);
                end = myDateTime.AddMonths(1).AddDays(-1).Day;
                if (selectYear == LunarDateTime.MinSupportedDateTime.Year
                    && selectMonth == LunarDateTime.MinSupportedDateTime.Month)
                {
                    start = LunarDateTime.MinSupportedDateTime.Day;
                }
                else if (selectYear == LunarDateTime.MaxSupportedDateTime.Year
                    && selectMonth == LunarDateTime.MaxSupportedDateTime.Month)
                {
                    end = LunarDateTime.MaxSupportedDateTime.Day;
                }
                for (int i = start; i <= end; i++)
                {
                    cboDay.Items.Add(string.Format(m_dayFormat, i));
                }
            }
            else if (m_state == 2)
            {
                int selectMonth = cboMonth.SelectedIndex + 1;
                int dayCount = LunarDateTime.GetDaysInMonth(selectYear, selectMonth);
                for (int i = 1; i <= dayCount; i++)
                {
                    cboDay.Items.Add(" " + LunarDateTime.GetLunarDayName(i));
                }
            }
        }

        /// <summary>
        /// 显示详情
        /// </summary>
        private void ShowDetial()
        {
            string strJieQi = LunarDateTime.GetJieQi();
            if (strJieQi.Contains(','))
            {
                strJieQi = strJieQi.Replace(",", " 至 ");
            }
            else
            {
                strJieQi = "今日" + strJieQi;
            }

            lblDetial.Text = string.Format(
                "{0}\r\n\r\n星座: {1} {2}\r\n\r\n{3}年 {4}月 {5}\r\n\r\n{6}{7}年 {8}{9}月 {10}{11}日\r\n\r\n节气: {12}\r\n\r\n",
                LunarDateTime.Gregorian.ToString("yyyy年MM月dd日 dddd"),//0

                LunarDateTime.GetXingZuoCH(),//1
                LunarDateTime.GetXingZuoEN(),//2

                LunarDateTime.GetLunarYearShengXiao(),//3
                LunarDateTime.GetLunarMonthName(),//4
                LunarDateTime.GetLunarDayName(),//5

                LunarDateTime.GetLunarYearTianGan(),//6
                LunarDateTime.GetLunarYearDiZhi(),//7
                LunarDateTime.GetLunarMonthTianGan(),//8
                LunarDateTime.GetLunarMonthDiZhi(),//9
                LunarDateTime.GetLunarDayTianGan(),//10
                LunarDateTime.GetLunarDayDiZhi(),//11

                strJieQi//LunarDateTime.GetJieQi()//12
                );
        }

        /// <summary>
        /// 双击复制信息到剪切板
        /// </summary>
        /// <param name="sender">事件源</param>
        /// <param name="e">事件参数</param>
        private void lblDetial_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(lblDetial.Text.Replace("\r\n\r\n", "\r\n"));
            }
            catch (Exception eexc)
            {
                MessageBox.Show(
                    "设置文本到剪切板失败：" + eexc.Message,
                    "异常信息");
            }
        }

        /// <summary>
        /// 打开帮助窗体
        /// </summary>
        /// <param name="sender">事件源</param>
        /// <param name="e">事件参数</param>
        private void btnHelp_Click(object sender, EventArgs e)
        {
            #region 初始化帮助文本
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("24节气：\r\n");
            for (int i = 0; i < LunarDateTime.JieQi.Length; i++)
            {
                strBuilder.Append(LunarDateTime.JieQi[i]);
                strBuilder.Append(" ");
            }
            strBuilder.Append("\r\n\r\n");

            strBuilder.Append("十天干：\r\n");
            for (int i = 0; i < LunarDateTime.TianGan.Length; i++)
            {
                strBuilder.Append(LunarDateTime.TianGan[i]);
                strBuilder.Append(" ");
            }
            strBuilder.Append("\r\n\r\n");

            strBuilder.Append("十二地支：\r\n");
            for (int i = 0; i < LunarDateTime.DiZhi.Length; i++)
            {
                strBuilder.Append(LunarDateTime.DiZhi[i]);
                strBuilder.Append(" ");
            }
            strBuilder.Append("\r\n\r\n");

            strBuilder.Append("十二生肖：\r\n");
            for (int i = 0; i < LunarDateTime.ShengXiao.Length; i++)
            {
                strBuilder.Append(LunarDateTime.ShengXiao[i]);
                strBuilder.Append(" ");
            }
            strBuilder.Append("\r\n\r\n");

            strBuilder.Append("十二星座：\r\n");

            string strFirst = null;
            string strLast = null;
            for (int i = 0; i < LunarDateTime.XingZuoCH.Length; i++)
            {
                strFirst = String.Format(
                    "{0}月{1}日",
                    i + 1,
                    LunarDateTime.XingZuoDay[i]);
                if (i != LunarDateTime.XingZuoCH.Length - 1)
                {
                    strLast = String.Format(
                        "{0}月{1}日",
                        i + 2,
                        LunarDateTime.XingZuoDay[i + 1]);
                }
                else
                {
                    strLast = String.Format(
                        "{0}月{1}日",
                        1,
                        LunarDateTime.XingZuoDay[0]);
                }

                strBuilder.Append(LunarDateTime.XingZuoCH[i]);
                strBuilder.Append("(");
                strBuilder.Append(LunarDateTime.XingZuoEN[i]);
                strBuilder.Append("):");
                strBuilder.Append(strFirst);
                strBuilder.Append("至");
                strBuilder.Append(strLast);
                strBuilder.Append("\r\n");
            }
            strBuilder.Append("\r\n\r\n");
            #endregion

            FrmHelpText frmHelp = new FrmHelpText();
            frmHelp.InnerText = strBuilder.ToString();
            frmHelp.ShowDialog();
        }
    }
}
