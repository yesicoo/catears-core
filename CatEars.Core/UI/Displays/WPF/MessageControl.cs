﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace CatEars.Core.UI.Displays.WPF
{
    /// <summary>
    /// 用于流水消息显示的 ItemsControl
    /// </summary>
    public class MessageControl
    {
        bool _canSelect;
        bool _canScroll;
        ItemsControl _itemControl;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemControl"></param>
        public MessageControl(ItemsControl itemControl)
        {
            _itemControl = itemControl;
            AutoSelect = true;
            ScrollIntoView = true;

            if(_itemControl is ListBox)
            {
                _canSelect = true;
                _canScroll = true;
            }
            else if (_itemControl is Selector)
            {
                _canSelect = true;
            }
        }

        /// <summary>
        /// 自动选中
        /// </summary>
        public bool AutoSelect { get; set; }

        /// <summary>
        /// 滚动至界面显示
        /// </summary>
        public bool ScrollIntoView { get; set; }

        /// <summary>
        /// 添加到开头
        /// </summary>
        public bool AppendToFirst { get; set; }
        /// <summary>
        /// 消息最大数量
        /// </summary>
        public int MaxCount { get; set; }

        /// <summary>
        /// 添加项到界面上
        /// </summary>
        /// <param name="objs"></param>
        /// <param name="isBatchEnd"></param>
        public void AddItem(IEnumerable objs, bool isBatchEnd)
        {
            AddItem(objs, isBatchEnd, AutoSelect, ScrollIntoView);
        }

        /// <summary>
        /// 添加项到界面上
        /// </summary>
        /// <param name="objs"></param>
        /// <param name="isBatchEnd"></param>
        /// <param name="autoSelect"></param>
        /// <param name="autoScrollIntoView"></param>
        void AddItem(IEnumerable objs, bool isBatchEnd, bool autoSelect, bool autoScrollIntoView)
        {
            lock (_itemControl)
            {
                int intNewItemIndex = 0;
                if (AppendToFirst)
                {
                    _itemControl.Items.Insert(0, objs);
                }
                else
                {
                    intNewItemIndex = _itemControl.Items.Add(objs);
                }
                if (MaxCount > 0)
                {
                    while (_itemControl.Items.Count > MaxCount)
                    {
                        _itemControl.Items.RemoveAt(_itemControl.Items.Count - 1);
                    }
                }
                if (isBatchEnd)
                {
                    if (_canSelect && autoSelect)
                    {
                        
                        ((Selector)_itemControl).SelectedIndex = intNewItemIndex;
                    }
                    if (_canScroll && autoScrollIntoView)
                    {
                        ((ListBox)_itemControl).ScrollIntoView(objs);
                    }
                }
            }
        }

        /// <summary>
        /// 清空已有消息
        /// </summary>
        /// <param name="wait">true同步 false异步</param>
        public void ClearMessage(bool wait = false)
        {
            lock (_itemControl)
            {
                Action action = () =>
                {
                    _itemControl.Items.Clear();
                };
                if (wait) Application.Current.Dispatcher.Invoke(action);
                else Application.Current.Dispatcher.BeginInvoke(action);
            }
        }
    }
}
