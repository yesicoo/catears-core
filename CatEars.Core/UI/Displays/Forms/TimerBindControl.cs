﻿using System;
using System.Windows.Forms;

namespace CatEars.Core.UI.Displays.Forms
{
    /// <summary>
    /// TimerBindControl使用的默认Action
    /// </summary>
    public static class TimerBindControlActions
    {
        /// <summary>
        /// StringToLabel
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="strText"></param>
        public static void StringToLabel(Control ctrl, string strText)
        {
            ctrl.Text = strText;
        }

        /// <summary>
        /// UpdateToolStripItemText
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="strText"></param>
        public static void UpdateToolStripItemText(ToolStripItem ctrl, string strText)
        {
            ctrl.Text = strText;
        }
    }

    /// <summary>
    /// 控件绑定Timer定时刷新
    /// </summary>
    /// <typeparam name="TControl"></typeparam>
    /// <typeparam name="TValue"></typeparam>
#pragma warning disable 1591
    public class TimerBindControl<TControl, TValue>
    {
        TControl _ctrl;
        Timer _timer;
        Action<TControl, TValue> _action;
        bool _valueFlag = false;
        TValue _value;
        
        public TimerBindControl(Action<TControl, TValue> action, TControl ctrl, Timer timer)
        {
            _action = action;
            _ctrl = ctrl;

            _timer = timer;
            _timer.Tick += _timer_Tick;
        }

        protected void _timer_Tick(object sender, EventArgs e)
        {
            if (_valueFlag)
            {
                _action(_ctrl, _value);
                _valueFlag = false;
            }
        }

        public void UpdateValue(TValue value)
        {
            _value = value;
            _valueFlag = true;
        }
    }
}
