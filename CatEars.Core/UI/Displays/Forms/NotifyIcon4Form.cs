﻿
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace CatEars.Core.UI.Displays.Forms
{
    /// <summary>
    /// NotifyIcon扩展
    /// </summary>
    public class NotifyIcon4Form
    {
        /// <summary>
        /// NotifyIcon
        /// </summary>
        public NotifyIcon NotifyIcon { get; private set; }

        /// <summary>
        /// 窗体引用
        /// </summary>
        public Form Form { get; private set; }

        /// <summary>
        /// 创建一个NotifyIconForForm对象
        /// </summary>
        /// <param name="frm">窗体</param>
        /// <param name="components">Form.components</param>
        /// <param name="icon">图标</param>
        /// <param name="contextMenuStrip">右键菜单</param>
        public NotifyIcon4Form(
            Form frm,
            IContainer components,
            Icon icon = null,
            ContextMenuStrip contextMenuStrip = null)
        {
            NotifyIcon = new NotifyIcon(components);
            NotifyIcon.Visible = true;
            NotifyIcon.Icon = icon;
            NotifyIcon.ContextMenuStrip = contextMenuStrip;
            NotifyIcon.Disposed += NotifyIcon_Disposed;
            NotifyIcon.DoubleClick += NotifyIcon_DoubleClick_ShowForm;
            Form = frm;
            Form.TextChanged += Form_TextChanged;
            Form_TextChanged(Form, EventArgs.Empty);
            _lastWindowState = Form.WindowState;
            if (_lastWindowState == FormWindowState.Minimized)
            {
                _lastWindowState = FormWindowState.Normal;
            }
        }

        void Form_TextChanged(object sender, EventArgs e)
        {
            if (NotifyIcon != null && Form != null)
            {
                NotifyIcon.Text = Form.Text;
            }
        }

        /// <summary>
        /// 释放资源时清除引用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void NotifyIcon_Disposed(object sender, EventArgs e)
        {
            NotifyIcon = null;
            Form = null;
        }

        /// <summary>
        /// 总是显示（false时窗体隐藏才显示）
        /// </summary>
        bool _blnAlwaysVisible = false;
        /// <summary>
        /// 总是显示（false时窗体隐藏才显示）
        /// </summary>
        public bool AlwaysVisible
        {
            get { return _blnAlwaysVisible; }
            set
            {
                _blnAlwaysVisible = value;
                if (NotifyIcon == null)
                {
                    return;
                }
                if (_blnAlwaysVisible)
                {
                    NotifyIcon.Visible = true;
                }
                else
                {
                    NotifyIcon.Visible = (Form.WindowState == FormWindowState.Minimized);
                }
            }
        }

        /// <summary>
        /// 绑定事件 只在窗体最小化的时候显示图标
        /// </summary>
        /// <param name="blnOpen">开启或关闭</param>
        public void BindEvent(bool blnOpen = true)
        {
            if (blnOpen)
            {
                if (Form != null)
                {
                    Form.Resize -= Form_Resize;
                    Form.Resize += Form_Resize;
                }
            }
            else
            {
                if (Form != null) Form.Resize -= Form_Resize;
            }
        }

        /// <summary>
        /// 提供给外部绑定显示事件的方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void NotifyIcon_DoubleClick_ShowForm(object sender, EventArgs e)
        {
            if (Form != null && NotifyIcon != null
                && Form.WindowState == FormWindowState.Minimized)
            {
                NotifyIcon.Visible = AlwaysVisible;
                Form.ShowInTaskbar = true;
                Form.TopMost = true;
                Form.WindowState = _lastWindowState;
                Form.Focus();
                Form.TopMost = false;
            }
        }

        /// <summary>
        /// 最后一次窗体大小缓存
        /// </summary>
        FormWindowState _lastWindowState;

        void Form_Resize(object sender, EventArgs e)
        {
            if (Form != null && NotifyIcon != null)
            {
                if (Form.WindowState == FormWindowState.Minimized)
                {
                    Form.ShowInTaskbar = false;
                    NotifyIcon.Visible = true;
                }
                else
                {
                    _lastWindowState = Form.WindowState;
                }
            }
        }
    }
}
