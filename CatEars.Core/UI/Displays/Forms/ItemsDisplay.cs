﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace CatEars.Core.UI.Displays.Forms
{
    /// <summary>
    /// 异步往列表添加对象
    /// </summary>
    public class ItemsDisplay<T> : ItemsDisplayBase<T>, IDisposable
    {
        Timer _timer;

        private bool _isNeedDispose = false;

        /// <summary>
        /// 更新间隔(毫秒)
        /// </summary>
        public override int Interval
        {
            get { return _timer.Interval; }
            set
            {
                int intValue = Math.Max(500, value);
                _timer.Interval = intValue;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        ~ItemsDisplay()
        {
            this.Dispose();
        }

        /// <summary>
        /// 初始化ItemsDisplay
        /// </summary>
        /// <param name="pushItem">将单个对象推给界面，第二个参数：是否是本批次最后一个对象</param>
        /// <param name="container">因为要创建timer，建议把窗体的IContainer传入</param>
        public ItemsDisplay(Action<T, bool> pushItem, IContainer container = null)
            : base(pushItem)
        {
            Init(container);
            _timer.Tick += PushItemTimerTick;
        }

        /// <summary>
        /// 初始化ItemsDisplay(批量版)
        /// </summary>
        /// <param name="pushItems">批量将对象推给界面</param>
        /// <param name="container">因为要创建timer，建议把窗体的IContainer传入</param>
        public ItemsDisplay(Action<ICollection<T>> pushItems, IContainer container = null)
            : base(pushItems)
        {
            Init(container);
            _timer.Tick += PushItemsTimerTick;
        }

        /// <summary>
        /// 初始化ItemsDisplay
        /// </summary>
        /// <param name="pushItem">将单个对象推给界面，第二个参数：是否是本批次最后一个对象</param>
        /// <param name="timer">指定一个外部的timer</param>
        public ItemsDisplay(Action<T, bool> pushItem, Timer timer)
            : base(pushItem)
        {
            _timer = timer;
            _timer.Tick += PushItemTimerTick;
        }

        /// <summary>
        /// 初始化ItemsDisplay(批量版)
        /// </summary>
        /// <param name="pushItems">批量将对象推给界面</param>
        /// /// <param name="timer">指定一个外部的timer</param>
        public ItemsDisplay(Action<ICollection<T>> pushItems, Timer timer)
            : base(pushItems)
        {
            _timer = timer;
            _timer.Tick += PushItemsTimerTick;
        }

        private void Init(IContainer container)
        {
            if (container != null)
            {
                _timer = new Timer(container);
            }
            else
            {
                _timer = new Timer();
            }
            Interval = 500;
            _isNeedDispose = true;
        }

        /// <summary>
        /// 开始Timer的运行
        /// </summary>
        public override void Start()
        {
            _timer.Start();
        }

        /// <summary>
        /// 停止Timer的运行
        /// </summary>
        public override void Stop()
        {
            _timer.Stop();
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (_timer != null)
            {
                if (_isNeedDispose) _timer?.Dispose();
                _timer = null;
            }
        }
    }
}
