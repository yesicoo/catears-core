﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace CatEars.Core.UI.Displays.Forms
{
    /// <summary>
    /// 消息列表
    /// </summary>
#pragma warning disable 1591
    public class MsgList : IDisposable
    {
        /// <summary>
        /// 用于显示的ListView控件
        /// </summary>
        public ListView ListView { get; private set; }

        /// <summary>
        /// 对象锁定
        /// </summary>
        public readonly object SyncRoot = new object();

        /// <summary>
        /// object为string或者string[]
        /// </summary>
        public ItemsDisplay<object> Appender { get; private set; }

        public MsgList(ListView listView, IContainer container = null)
        {
            MaxCount = 500;
            ListView = listView;
            Appender = new ItemsDisplay<object>(UpdateList, container);
        }

        public MsgList(ListView listView, Timer timer)
        {
            MaxCount = 500;
            ListView = listView;
            Appender = new ItemsDisplay<object>(UpdateList, timer);
        }

        public void Dispose()
        {
            IDisposable obj = Appender;
            if (obj != null) obj.Dispose();
            ListView = null;
        }

        private void UpdateList(ICollection<object> items)
        {
            int intMaxCount = MaxCount;
            if (intMaxCount <= 0)
            {
                ClearMessage();
                return;
            }
            var list = items.ToArray();
            lock (SyncRoot)
            {
                if (this.AppendToFirst)
                {
                    if (ListView.Items.Count >= intMaxCount)
                    {
                        //已有的Item到达限制值时，只改变ListViewItem的文本 不修改ListView.Item
                        for (int i = ListView.Items.Count - list.Length - 1; i >= 0; i--)
                        {
                            var item0 = ListView.Items[i];
                            var item1 = ListView.Items[i + list.Length];
                            ListViewItemValueCopy(item0, item1);
                        }
                        for (int i = 0; i < list.Length; i++)
                        {
                            var item0 = ListView.Items[i];
                            //需要倒过来 最后一项在最前面
                            int intIndex = list.Length - i - 1;
                            ListViewItemUpdate(item0, list[intIndex]);
                        }
                    }
                    else
                    {
                        //在0位置插入消息
                        for (int i = 0; i < list.Length; i++)
                        {
                            var newItem = ListView.Items.Insert(0, "");
                            ListViewItemUpdate(newItem, list[i]);
                        }
                    }
                    //删除多余消息
                    while (ListView.Items.Count > intMaxCount)
                    {
                        ListView.Items.RemoveAt(intMaxCount);
                    }
                    //ListView.Items[0].EnsureVisible();
                }
                else
                {
                    if (ListView.Items.Count >= intMaxCount)
                    {
                        //已有的Item到达限制值时，只改变ListViewItem的文本 不修改ListView.Item
                        for (int i = list.Length; i < ListView.Items.Count; i++)
                        {
                            var item0 = ListView.Items[i];
                            var item1 = ListView.Items[i - list.Length];
                            ListViewItemValueCopy(item0, item1);
                        }
                        int intStart = ListView.Items.Count - list.Length;
                        for (int i = 0; i < list.Length; i++)
                        {
                            var item0 = ListView.Items[intStart + i];
                            ListViewItemUpdate(item0, list[i]);
                        }
                    }
                    else
                    {
                        //顺序在末位插入消息
                        for (int i = 0; i < list.Length; i++)
                        {
                            var newItem = ListView.Items.Add("");
                            ListViewItemUpdate(newItem, list[i]);
                        }
                    }
                    //删除多余消息
                    while (ListView.Items.Count > intMaxCount)
                    {
                        ListView.Items.RemoveAt(0);
                    }
                    //ListView.Items[ListView.Items.Count - 1].EnsureVisible();
                }
            }
        }

        private void ListViewItemValueCopy(ListViewItem itemForm, ListViewItem itemTo)
        {
            if (itemTo.SubItems.Count == itemTo.SubItems.Count)
            {
                for (int i = 1; i < itemForm.SubItems.Count; i++)
                {
                    itemTo.SubItems[i].Text = itemForm.SubItems[i].Text;
                }
            }
            else
            {
                itemTo.SubItems.Clear();
                for (int i = 1; i < itemForm.SubItems.Count; i++)
                {
                    itemTo.SubItems.Add(itemForm.SubItems[i].Text);
                }
            }
            itemTo.Text = itemForm.Text;
            //itemTo.Tag = itemForm.Tag;
        }

        private void ListViewItemUpdate(ListViewItem item, object objValue)
        {
            string strMsg = objValue as string;
            string[] strMsgs = objValue as string[];
            if (strMsg != null)
            {
                item.Text = strMsg;
            }
            else if (strMsgs != null && strMsgs.Length > 0)
            {
                if (item.SubItems.Count == strMsgs.Length)
                {
                    for (int i = 1; i < item.SubItems.Count; i++)
                    {
                        item.SubItems[i].Text = strMsgs[i];
                    }
                }
                else
                {
                    item.SubItems.Clear();
                    for (int i = 1; i < strMsgs.Length; i++)
                    {
                        item.SubItems.Add(strMsgs[i]);
                    }
                }
                item.Text = strMsgs[0];
            }
        }

        #region 接口实现

        public int UpdateInterval
        {
            get { return Appender.Interval; }
            set { Appender.Interval = value; }
        }

        public bool AppendToFirst { get; set; }

        public int MaxCount { get; set; }

        public void AddMsg(string strMsg)
        {
            Appender.Add(strMsg);
        }

        public void ClearMessage()
        {
            lock (SyncRoot)
            {
                ListView.Items.Clear();
            }
        }

        #endregion

        void AddMsg(string[] strMsg)
        {
            Appender.AddRange(strMsg);
        }

        public ColumnHeader AddTimeCol()
        {
            return AddCol("时间", 132);
        }

        public ColumnHeader AddMsgCol()
        {
            return AddCol("消息", 10000);
        }

        public ColumnHeader AddCol(string strColName, int intWidth = 200)
        {
            var col = new ColumnHeader();
            col.Name = strColName;
            col.Text = strColName;
            col.Width = intWidth;
            ListView.Columns.Add(col);
            return col;
        }
    }
}
