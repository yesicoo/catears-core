﻿using System.IO;
using CatEars.Core.Console;

namespace CatEars.Demo.Extension.IO
{
    public static class Test_ExPath
    {
        #region 测试路径格式化功能
        public static void PathFormat()
        {
            PathFormat("D:/Hello.c");
            PathFormat("Hello.cpp");
        }

        static void PathFormat(string path)
        {
            ColorConsole.WriteWarnLine($"Path = \"{path}\"");
            ColorConsole.WriteWarnLine($"CPath.ToFullFilePath = \"{path.ToFullFilePath()}\"");
            ColorConsole.WriteWarnLine($"CPath.GetFullDirectoryPath = \"{path.GetFullDirectoryPath()}\"");
            ColorConsole.WriteErrorLine($"CPath.ToFullDirectoryPath = \"{path.ToFullDirectoryPath()}\"");
        }
        #endregion
    }
}
