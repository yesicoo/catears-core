﻿using System;
using System.Threading;
using CatEars.Core;
using CatEars.Core.Console;

namespace CatEars.Demo.Core.Console
{
    public static class Test_ColorConsole
    {
        #region 多线程读写Console
        public static void MultiThreadTest()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread t = new Thread(MultiThreadTest_Action);
                t.Start($"Thread no.{i}");
            }
            string input = ColorConsole.AskInput("Input something : ");
            ColorConsole.WriteSuccessLine("Result : " + input);
        }

        static void MultiThreadTest_Action(object obj)
        {
            Thread.Sleep(AppEnv.Random.Next(2000, 5000));
            ColorConsole.WriteInfoLine(obj.ToString());
        }
        #endregion
        
        /// <summary>
        /// 显示所有颜色
        /// </summary>
        public static void Debug_PrintAllColor()
        {
            System.Console.WriteLine("------------------------- Start -------------------------");
            ConsoleColor foreColorBak = System.Console.ForegroundColor;
            ConsoleColor backColorBak = System.Console.BackgroundColor;
            try
            {
                Array values = Enum.GetValues(typeof(ConsoleColor));
                foreach (ConsoleColor value in values)
                {
                    string strName = value.ToString();
                    if (strName == "Black")
                    {
                        System.Console.BackgroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        System.Console.BackgroundColor = ConsoleColor.Black;
                    }
                    System.Console.ForegroundColor = value;
                    System.Console.WriteLine("This line is printed with the ForegroundColor of ConsoleColor." + value);
                }
            }
            finally
            {
                System.Console.BackgroundColor = backColorBak;
                System.Console.ForegroundColor = foreColorBak;
            }
            System.Console.WriteLine("-------------------------  End  -------------------------");
            System.Console.WriteLine();
        }

        /// <summary>
        /// 显示所有背景色
        /// </summary>
        public static void Debug_PrintAllBackColor()
        {
            System.Console.WriteLine("------------------------- Start -------------------------");
            ConsoleColor foreColorBak = System.Console.ForegroundColor;
            ConsoleColor backColorBak = System.Console.BackgroundColor;
            try
            {
                Array values = Enum.GetValues(typeof(ConsoleColor));
                foreach (ConsoleColor value in values)
                {
                    string strName = value.ToString();
                    if (strName == "Black")
                    {
                        System.Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        System.Console.ForegroundColor = ConsoleColor.Black;
                    }
                    System.Console.BackgroundColor = value;
                    System.Console.WriteLine("This line is printed with the BackgroundColor of ConsoleColor." + value);
                }
            }
            finally
            {
                System.Console.BackgroundColor = backColorBak;
                System.Console.ForegroundColor = foreColorBak;
            }
            System.Console.WriteLine("-------------------------  End  -------------------------");
            System.Console.WriteLine();
        }

        /// <summary>
        /// 测试所有Write方法
        /// </summary>
        public static void Debug_TestAllWriteMethod()
        {
            ColorConsoleEx ex = ColorConsoleEx.WithTimeAndTypeName;
            ex.WriteDebugLine("Test WriteDebug()");
            ex.WriteInfoLine("Test WriteInfo()");
            ex.WriteWarnLine("Test WriteWarnLine()");
            ex.WriteErrorLine("Test WriteErrorLine()");
            ex.WriteFatalLine("Test WriteFatalLine()");
            ex.WriteSuccessLine("Test WriteSuccessLine()");

            ColorConsoleEx.EnterToContinue();
        }
    }
}
