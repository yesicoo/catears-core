﻿using System.Threading;
using CatEars.Core.Console;
using CatEars.Core.Utility;

namespace CatEars.Demo.Core.Utility
{
    public static class Test_PerformanceTestInConsole
    {
        public static void PrintPerformanceTestDemo()
        {
            PerformanceTestInConsole.Initialize();
            PerformanceTestInConsole.Time("PrintPerformanceTestDemo", 10, PrintPerformanceTestDemo_Action);
            PerformanceTestInConsole.Time("PrintPerformanceTestDemo", 50, PrintPerformanceTestDemo_Action);
            PerformanceTestInConsole.Time("PrintPerformanceTestDemo", 100, PrintPerformanceTestDemo_Action);
        }
        static void PrintPerformanceTestDemo_Action()
        {
            ColorConsole.WriteSuccess(" * ");
            Thread.Sleep(10);
        }
    }
}
