﻿using System;
using System.Collections.Generic;
using CatEars.Core;
using CatEars.Core.Console;
using CatEars.Demo.Core;
using CatEars.Demo.Core.Console;
using CatEars.Demo.Core.Utility;
using CatEars.Demo.Extension.IO;

namespace CatEars.Demo
{
    class Program
    {
        /// <summary>
        /// 标记是否退出
        /// </summary>
        static bool _exit;

        static void Main(string[] args)
        {
            try
            {
                AppEnv.InitConsole();

                _exit = false;
                var list = GetDemoList();
                //当前页数
                int page = 0;
                //每页显示项
                int pageItem = 18;
                //可用项目从keep开始编号
                int keep = 3;
                while (!_exit)
                {
                    ColorConsole.WriteInfoLine(null);
                    //显示菜单
                    ColorConsole.WriteWarnLine($"    Current page : {page}");
                    ColorConsole.WriteWarnLine($"{ExString.FillLeft("0", 4)} -- Exit");
                    ColorConsole.WriteWarnLine($"{ExString.FillLeft("1", 4)} -- Page Up");
                    ColorConsole.WriteWarnLine($"{ExString.FillLeft("2", 4)} -- Page Down");
                    for (int i = 0; i < pageItem; i++)
                    {
                        int index = page * pageItem + i;
                        if (index >= list.Count) break;
                        ColorConsole.WriteInfoLine($"{ExString.FillLeft((i + keep).ToString(), 4)} -- {list[index].Item1}");
                    }
                    Input:
                    int input;
                    ColorConsole.AskInput<int>("输入序号：", out input);
                    if (input == 0) Exit();
                    else if (input == 1) page = Math.Max(0, page - 1);
                    else if (input == 2) page = Math.Min(((list.Count - 1) / pageItem), page + 1);
                    else
                    {
                        var itemIndex = page * pageItem + input - keep;
                        if (itemIndex > list.Count) continue;
                        var item = list[itemIndex];
                        if (item.Item2 == null) goto Input;
                        try
                        {
                            item.Item2.Invoke();
                        }
                        catch (Exception exc)
                        {
                            AppEnv.Log.Error(exc);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                AppEnv.Log.Fatal(exc);
                ColorConsoleEx.EnterToExit();
            }
            finally
            {
                //ColorConsoleEx.EnterToExit();
            }
        }
        static void Exit()
        {
            _exit = true;
        }

        static string _line = "----- ----- ----- ----- ----- ----- ----- ----- ----- -----";

        static List<Tuple<string, Action>> GetDemoList()
        {
            //Demo列表
            var result = new List<Tuple<string, Action>>();
            
            result.Add(new Tuple<string, Action>("AppEnv    显示程序相关路径", Test_AppEnv.PrintCAppEnvPath));
            result.Add(new Tuple<string, Action>(_line, null));
            result.Add(new Tuple<string, Action>("Test_CPath    路径转换测试", Test_ExPath.PathFormat));
            result.Add(new Tuple<string, Action>(_line, null));
            result.Add(new Tuple<string, Action>("PerformanceTestInConsole    显示性能测试输出格式", Test_PerformanceTestInConsole.PrintPerformanceTestDemo));
            result.Add(new Tuple<string, Action>(_line, null));
            result.Add(new Tuple<string, Action>("ColorConsole    多线程测试", Test_ColorConsole.MultiThreadTest));
            result.Add(new Tuple<string, Action>("ColorConsole    显示所有背景色", Test_ColorConsole.Debug_PrintAllBackColor));
            result.Add(new Tuple<string, Action>("ColorConsole    显示所有前景色", Test_ColorConsole.Debug_PrintAllColor));
            result.Add(new Tuple<string, Action>("ColorConsole    显示所有Write方法的效果", Test_ColorConsole.Debug_TestAllWriteMethod));

            return result;
        }
    }
}
