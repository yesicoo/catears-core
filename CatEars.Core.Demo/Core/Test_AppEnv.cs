﻿using CatEars.Core;
using CatEars.Core.Console;

namespace CatEars.Demo.Core
{
    public static class Test_AppEnv
    {
        public static void PrintCAppEnvPath()
        {
            ColorConsole.WriteInfoLine($"AppEnv.ExeFilePath = \"{AppEnv.ExeFilePath}\"");
            ColorConsole.WriteInfoLine($"AppEnv.ExeDirPath = \"{AppEnv.ExeDirPath}\"");
            ColorConsole.WriteInfoLine($"AppEnv.CurrentDirectory = \"{AppEnv.CurrentDirectory}\"");
        }
    }
}
